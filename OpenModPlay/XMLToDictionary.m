//
//  XMLToDictionary.m
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-13.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import "XMLToDictionary.h"

@implementation XMLToDictionary {
    NSMutableDictionary* _dictionary;
    NSMutableArray* _stack;
    NSMutableDictionary* _currentElement;
    NSString* _currentName;
    NSString* _currentText;
}

-(id)init {
    self = [super init];
    _dictionary = [NSMutableDictionary dictionary];
    return self;
}

-(NSDictionary*)dictionary {
    return _dictionary;
}

-(void)parserDidStartDocument:(NSXMLParser *)parser {
    [_dictionary removeAllObjects];
    _stack = [NSMutableArray arrayWithObject:@{@"name":@"root", @"element":_dictionary}];
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary<NSString *,NSString *> *)attributeDict {

    _currentElement = [NSMutableDictionary dictionary];
    _currentName = elementName;
    [_stack addObject:@{@"name":_currentName, @"element":_currentElement}];
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    [_stack removeLastObject];
    
    // Demote current element to NSString if it has no children
    id newElement = _currentElement.count==0 ? [_currentText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] : _currentElement;

    NSMutableDictionary* parent = _stack.lastObject[@"element"];
    NSString* parentName = _stack.lastObject[@"name"];
    id existing = parent[_currentName];
    if (!existing)
        [parent setObject:newElement forKey:_currentName];
    else {
        if ([existing isKindOfClass:[NSMutableArray class]])
            [existing addObject:newElement];
        else {
            // Promote to NSArray
            NSMutableArray* array = [NSMutableArray array];
            [array addObject:existing];
            [array addObject:newElement];
            [parent removeObjectForKey:_currentName];
            [parent setObject:array forKey:_currentName];
        }
    }

    _currentElement = parent;
    _currentName = parentName;
}

-(void)parser:(NSXMLParser *)parser foundCDATA:(NSData *)CDATABlock {
    _currentText = [[NSString alloc] initWithData:CDATABlock encoding:NSUTF8StringEncoding];
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    _currentText = string;
}

@end
