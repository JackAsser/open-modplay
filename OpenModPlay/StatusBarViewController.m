//
//  StatusBarViewController.m
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-11.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import "StatusBarViewController.h"
#import "Playback.h"
#import "PopupViewController.h"
#import "Utilities.h"
#import "Database.h"
#import <MediaPlayer/MediaPlayer.h>

@implementation StatusBarViewController {
    IBOutlet UIProgressView* progress;
    IBOutlet UIButton* playPauseButton;
    BOOL wirelessRoutesAvailable;
    IBOutlet UILabel* titleAndArtistLabel;
    IBOutlet UILabel* artistLabel;
    IBOutlet UILabel* airplayAvailableLabel;
    MPVolumeView* volumeView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    wirelessRoutesAvailable = NO;
    
    progress.progress = 0;
    [[NSNotificationCenter defaultCenter] addObserverForName:PlaybackProgressNotification object:[Playback sharedInstance] queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        double duration = openmpt_module_get_duration_seconds([Playback sharedInstance].currentModule);
        double position = openmpt_module_get_position_seconds([Playback sharedInstance].currentModule);
        progress.progress = (float)(position / duration);
    }];

    [[NSNotificationCenter defaultCenter] addObserverForName:PlaybackStateChangedNotification object:[Playback sharedInstance] queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        [self updateUI];
    }];

    [[NSNotificationCenter defaultCenter] addObserverForName:PlaybackNewModuleNotification object:[Playback sharedInstance] queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        [self updateUI];
    }];

    artistLabel.textColor = [UIColor lightGrayColor];

    volumeView = [[MPVolumeView alloc] init];
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(checkWirelessRoutes) userInfo:nil repeats:YES];
    [self updateUI];
}

- (void)checkWirelessRoutes {
    if (volumeView.wirelessRoutesAvailable != wirelessRoutesAvailable) {
        wirelessRoutesAvailable = volumeView.wirelessRoutesAvailable;
        [self performSelectorOnMainThread:@selector(updateUI) withObject:nil waitUntilDone:NO];
    }
}

- (void)updateUI {    
    if ([Playback sharedInstance].playing)
        [playPauseButton setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
    else
        [playPauseButton setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    
    NSString* title = @"";
    NSString* artist = @"";

    CKRecord* moduleRecord = [Playback sharedInstance].currentModuleRecord;
    if (moduleRecord) {
        title = moduleRecord[@"title"];
        artist = [Utilities artistStringForModuleRecord:moduleRecord];
    }
    
    if (wirelessRoutesAvailable) {
        airplayAvailableLabel.hidden = NO;
        artistLabel.hidden = YES;
        
        if ((title.length>0) && (artist.length>0))
            titleAndArtistLabel.text = [NSString stringWithFormat:@"%@ • %@", title, artist];
        else if (title.length>0)
            titleAndArtistLabel.text = title;
        else
            titleAndArtistLabel.text = @"";
    } else {
        airplayAvailableLabel.hidden = YES;
        artistLabel.hidden = NO;
        
        if (title.length>0)
            titleAndArtistLabel.text = title;
        else
            titleAndArtistLabel.text = @"";
        
        if (artist.length>0)
            artistLabel.text = artist;
        else
            artistLabel.text = @"";
    }
}

- (IBAction)playPausePressed:(id)sender {
    [[Playback sharedInstance] togglePlay];
}

- (IBAction)expandButtonPressed:(id)sender {
    [(PopupViewController*)self.parentViewController expand];
}

- (IBAction)handleDrag:(UIPanGestureRecognizer *)recognizer {
    [(PopupViewController*)self.parentViewController handleDrag:recognizer];
}

@end
