//
//  ModArchive.h
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-13.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Promise.h"

@interface ModArchive : NSObject

+ (ModArchive* _Nonnull)sharedInstance;

- (Promise* _Nonnull)searchForModule:(NSString* _Nonnull)searchString page:(NSUInteger)page;
- (Promise* _Nonnull)searchForArtist:(NSString* _Nonnull)searchString page:(NSUInteger)page;
- (Promise* _Nonnull)modulesByArtist:(NSString* _Nonnull)artistID page:(NSUInteger)page;
- (Promise* _Nonnull)downloadModuleById:(NSString * _Nonnull)_id;
- (Promise* _Nonnull)getModuleByHash:(NSString* _Nonnull)hash;

@end
