//
//  SearchResultsTableViewController.m
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-13.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import "SearchResultsTableViewController.h"
#import "ModArchive.h"
#import "ArtistTableViewController.h"
#import "Utilities.h"
#import "SongTableViewCell.h"
#import "Database.h"
#import "Playback.h"

#define MAX_SONGS 4
#define MAX_ARTISTS 4

@implementation SearchResultsTableViewController {
    NSArray* modules;
    NSArray* artists;
    UIImage* ellipsisImage;
    NSString* currentSearch;
    NSString* pendingSearch;
    NSString* searchStringForResults;
    Playlist* playlist;
    id stateChangeObserver;
    id newModuleObserver;
}

enum SectionType {
    SECTION_MODULES,
    SECTION_ARTISTS,
    SECTION_UNKNOWN
};

- (void)viewDidLoad {
    [super viewDidLoad];
    modules = nil;
    artists = nil;
    ellipsisImage = [[UIImage imageNamed:@"ellipsis"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    stateChangeObserver = [[NSNotificationCenter defaultCenter] addObserverForName:PlaybackStateChangedNotification object:[Playback sharedInstance] queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        [self.tableView reloadData];
    }];
    
    newModuleObserver = [[NSNotificationCenter defaultCenter] addObserverForName:PlaybackNewModuleNotification object:[Playback sharedInstance] queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        [self.tableView reloadData];
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (stateChangeObserver) {
        stateChangeObserver = nil;
        [[NSNotificationCenter defaultCenter] removeObserver:stateChangeObserver];
    }

    if (newModuleObserver) {
        newModuleObserver = nil;
        [[NSNotificationCenter defaultCenter] removeObserver:newModuleObserver];
    }
}

- (enum SectionType)typeForSection:(NSInteger)section {
    if ((section == 0) && modules)
        return SECTION_MODULES;
    else if ((((section == 0) && !modules) || (section == 1)) && artists)
        return SECTION_ARTISTS;
    else
        return SECTION_UNKNOWN;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return (modules?1:0) + (artists?1:0);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    enum SectionType type = [self typeForSection:section];
    switch (type) {
        case SECTION_MODULES:
            return MIN(MAX_SONGS,modules.count) + ((modules.count>MAX_SONGS)?1:0);
        case SECTION_ARTISTS:
            return MIN(MAX_ARTISTS,artists.count) + ((artists.count>MAX_ARTISTS)?1:0);
        default:
            return 0;
    }
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    enum SectionType type = [self typeForSection:section];
    switch (type) {
        case SECTION_MODULES:
            return @"Songs";
        case SECTION_ARTISTS:
            return @"Artists";
        default:
            return @"";
    }
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.textColor = [UIColor whiteColor];
    header.textLabel.font = [UIFont boldSystemFontOfSize:18];
    CGRect headerFrame = header.frame;
    header.textLabel.frame = headerFrame;
    header.textLabel.textAlignment = NSTextAlignmentCenter;
    header.textLabel.text = [header.textLabel.text capitalizedString];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    enum SectionType type = [self typeForSection:indexPath.section];
    
    switch (type) {
        case SECTION_MODULES: {
            if (indexPath.row<MAX_SONGS) {
                SongTableViewCell* stvc = [tableView dequeueReusableCellWithIdentifier:@"SONG"];
                stvc.viewController = self;
                stvc.module = playlist.list[indexPath.row];
                cell = stvc;
            } else {
                cell = [tableView dequeueReusableCellWithIdentifier:@"SHOW_ALL_SONGS"];
                cell.textLabel.textColor = [UIColor whiteColor];
                cell.detailTextLabel.textColor = [UIColor lightGrayColor];
                cell.backgroundColor = [UIColor clearColor];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            break;
        }
        case SECTION_ARTISTS: {
            if (indexPath.row<MAX_ARTISTS) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"ARTIST"];
                cell.textLabel.text = [artists[indexPath.row][@"alias"] capitalizedString];
            } else
                cell = [tableView dequeueReusableCellWithIdentifier:@"SHOW_ALL_ARTISTS"];
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.detailTextLabel.textColor = [UIColor lightGrayColor];
            cell.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

            break;
        }
        default:
            break;
    }
    
    return cell;
}

- (void)doSearch:(NSString*)searchString {
    if (currentSearch) {
        pendingSearch = searchString;
        return;
    }
    currentSearch = searchString;
    
    Promise* moduleSearch = [[[[ModArchive sharedInstance] searchForModule:searchString page:0] onMainQueue] when:^(id results) {
        id tmp = results[@"modarchive"][@"module"];
        if (tmp == nil)
            modules = nil;
        else if ([tmp isKindOfClass:[NSArray class]])
            modules = tmp;
        else
            modules = @[tmp];
    } failed:^(NSError *error) {
        modules = nil;
        NSLog(@"Error: %@", error);
    } any:^{
        searchStringForResults = searchString;
        [Utilities generateModuleTitles:[modules mutableCopy]];
        modules = [Utilities sortModulesByTitle:modules];
        
        playlist = [[Playlist alloc] initWithName:@"search results"];
        for (NSDictionary* moduleInfo in modules) {
            CKRecord* module = [[Database sharedInstance] importModuleFromModArchive:moduleInfo addToLibrary:NO];
            [playlist.list addObject:module];
        }

        [self.tableView reloadData];
    }];
    
    Promise* artistSearch = [[[[ModArchive sharedInstance] searchForArtist:searchString page:0] onMainQueue] when:^(id results) {
        id tmp = results[@"modarchive"][@"items"][@"item"];
        if (tmp == nil)
            artists = nil;
        else if ([tmp isKindOfClass:[NSArray class]])
            artists = tmp;
        else
            artists = @[tmp];
    } failed:^(NSError *error) {
        artists = nil;
        NSLog(@"Error: %@", error);
    } any:^{
        searchStringForResults = searchString;
        [Utilities generateArtistNames:[artists mutableCopy]];
        artists = [Utilities sortArtistsByName:artists];
        [self.tableView reloadData];
    }];
    
    [[[Promise all:@[moduleSearch, artistSearch]] onMainQueue] any:^{
        if (pendingSearch) {
            NSString* searchString = pendingSearch;
            currentSearch = nil;
            pendingSearch = nil;
            [self doSearch:searchString];
        } else
            currentSearch = nil;
    }];
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString* searchString = searchController.searchBar.text;
    if (searchString.length == 0)
        return;
    [self doSearch:searchString];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    enum SectionType type = [self typeForSection:indexPath.section];
    switch (type) {
        case SECTION_MODULES: {
            [[Playback sharedInstance] setPlaylist:playlist position:indexPath.row];
            [[Playback sharedInstance] play];
            break;
        }
        case SECTION_ARTISTS: {
            ArtistTableViewController* artist = [self.storyboard instantiateViewControllerWithIdentifier:@"ARTISTTABLEVIEW"];
            artist.artistID = artists[indexPath.row][@"id"];
            artist.name = artists[indexPath.row][@"name"];
            artist.navigationItem.title = artists[indexPath.row][@"name"];
            [self.parentViewController.presentingViewController.navigationController pushViewController:artist animated:YES];
            break;
        }
        default:
            break;
    }
}

@end
