//
//  Utilities.m
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-15.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import "Utilities.h"
#include <libopenmpt/libopenmpt.h>
#import "Database.h"

@implementation Utilities

static NSMutableDictionary* albumArts;
+ (void)initialize {
    albumArts = [NSMutableDictionary dictionary];
}

+ (NSString*) titleStringForModule:(NSDictionary *)moduleInfo {
    NSString* title = moduleInfo[@"title"];
    if (title)
        return title;
    title = [[self removeHTMLinString:moduleInfo[@"songtitle"]] capitalizedString];
    
    if ([[title lowercaseString] hasPrefix:@"mod."])
        title = [title substringFromIndex:4];

    // Uppercase roman numerals
    NSString *romanRegex = @"^(?=.)(?i)M*(D?C{0,3}|C[DM])(L?X{0,3}|X[LC])(V?I{0,3}|I[VX])$";
    NSPredicate *romanTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", romanRegex];
    NSArray* words = [title componentsSeparatedByString:@" "];
    NSMutableArray* newWords = [NSMutableArray array];
    for (NSString* word in words) {
        NSString* tmp = [word uppercaseString];
        if ([romanTest evaluateWithObject:tmp])
            [newWords addObject:tmp];
        else
            [newWords addObject:word];
    }
    title = [newWords componentsJoinedByString:@" "];
    
    return title;
}

+ (NSString*) titleStringForOpenMPTModule:(openmpt_module *)module {
    NSString* title = [NSString stringWithCString:openmpt_module_get_metadata(module, "title") encoding:NSASCIIStringEncoding];
    return [title capitalizedString];
}

+ (NSString*) artistStringForOpenMPTModule:(openmpt_module *)module {
    NSString *artist = [NSString stringWithCString:openmpt_module_get_metadata(module, "artist") encoding:NSASCIIStringEncoding];

    if (!artist || (artist.length==0))
        return nil;
    
    return [artist capitalizedString];
}

+ (NSString*) nameStringForArtist:(NSDictionary *)artistInfo {
    NSString* name = artistInfo[@"name"];
    if (name)
        return name;
    name = [[self removeHTMLinString:artistInfo[@"alias"]] capitalizedString];
    return name;
}

+ (NSArray<NSString*>*) artistsForModule:(NSDictionary*)moduleInfo includeGuessed:(BOOL)guessed {
    NSMutableArray* list = [NSMutableArray array];
    NSDictionary* artistInfo = moduleInfo[@"artist_info"];
    
    NSInteger nbrArtists = [artistInfo[@"artists"] integerValue];
    NSInteger nbrGuessedArtists = guessed ? [artistInfo[@"guessed_artists"] integerValue] : 0;
    
    NSMutableSet<NSString*>* alreadyAdded = [NSMutableSet set];
    
    for (int i=0; i<MAX(nbrArtists, nbrGuessedArtists); i++) {
        NSString* artist = nil;
        NSNumber* _id = nil;
        
        if (i<nbrArtists) {
            if (nbrArtists > 1) {
                artist = artistInfo[@"artist"][i][@"alias"];
                _id = artistInfo[@"artist"][i][@"id"];
            }
            else {
                artist = artistInfo[@"artist"][@"alias"];
                _id = artistInfo[@"artist"][@"id"];
            }
        } else if (i<nbrGuessedArtists) {
            if (nbrGuessedArtists > 1)
                artist = artistInfo[@"guessed_artist"][i][@"alias"];
            else
                artist = artistInfo[@"guessed_artist"][@"alias"];
        }
        
        if (artist) {
            NSString* name = [[self removeHTMLinString:artist] capitalizedString];
            
            NSString* alias = [name lowercaseString];
            if (![alreadyAdded containsObject:alias]) {
                [alreadyAdded addObject:alias];
                if (_id)
                    [list addObject:@{@"name": name,
                                      @"id": _id
                                      }];
                else
                    [list addObject:@{@"name": name}];
            }
        }
    }

    return list;
}

+ (NSString*)joinedName:(NSArray<NSString*>*) names {
    NSMutableArray<NSString*>* list = [names mutableCopy];
    if (list.count==0)
        return @"";
    else if (list.count==1)
        return list[0];
    else if (list.count==2)
        return [list componentsJoinedByString:@" & "];
    else {
        NSString* last = [list lastObject];
        [list removeLastObject];
        return [NSString stringWithFormat:@"%@ & %@", [list componentsJoinedByString:@", "], last];
    }
}

+ (NSString*) artistStringForModule:(NSDictionary *)moduleInfo {
    NSMutableArray<NSString*>* list = [NSMutableArray array];

    for (NSDictionary* dict in [self artistsForModule:moduleInfo includeGuessed:YES])
        [list addObject:dict[@"name"]];

    return [self joinedName:list];
}

+ (NSString*) artistStringForModuleRecord:(CKRecord*)moduleRecord {
    NSMutableArray<NSString*>* list = [NSMutableArray array];
    
    NSArray* artists = moduleRecord[@"artists"];
    for (CKReference* ref in artists) {
        CKRecordID* recordID = ref.recordID;
        if (!recordID)
            continue;
        CKRecord* record = [[Database sharedInstance] findArtistByRecordID:recordID];
        if (record)
            [list addObject:record[@"name"]];
    }
   // NSLog(@"%@ -> %@", list, [self joinedName:list]);
    
    return [self joinedName:list];
}

+ (UIImage*) albumArtForModule:(openmpt_module*)module record:(CKRecord*)record {
    NSString* hash = record[@"hash"];

    UIImage* image = albumArts[hash];
    if (image)
        return image;
    
    NSString* format = record[@"format"];
    NSInteger nbrChannels = openmpt_module_get_num_channels(module);
    NSString* title = record[@"title"];
    NSString* artist = [self artistStringForModuleRecord:record];
    
    NSLog(@"%@, %@, %ld, %@, %@", hash, format, (long)nbrChannels, title, artist);

    CGRect bounds = CGRectMake(0, 0, 1024, 1024);
    UIGraphicsBeginImageContext(bounds.size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGFloat spreadH = 0.2;
    CGFloat baseH = (CGFloat)arc4random() / UINT32_MAX;
    baseH *= (1.0-spreadH);
    baseH += spreadH/2.0;
    
    for (int i=0; i<300; i++) {
        CGFloat h = (CGFloat)arc4random() / UINT32_MAX;
        h*=spreadH;
        h-=spreadH/2.0;
        h+=baseH;
        CGFloat s = 0.3;
        CGFloat b = 0.7;
        UIColor* color = [UIColor colorWithHue:h saturation:s brightness:b alpha:1.0];
        CGContextSetFillColorWithColor(ctx, color.CGColor);

        if (i==0) {
            CGContextFillRect(ctx, bounds);
            continue;
        }

        CGFloat size = ((CGFloat)arc4random() / UINT32_MAX) * 200 + 150;
        CGFloat x = ((CGFloat)arc4random() / UINT32_MAX) * bounds.size.width;
        CGFloat y = ((CGFloat)arc4random() / UINT32_MAX) * bounds.size.height;
        CGRect ellipse = CGRectMake(x-size/2, y-size/2, size, size);
        CGContextFillEllipseInRect(ctx, ellipse);
    }
    
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceRGB();
    CGColorRef gradientStart = [UIColor colorWithWhite:0 alpha:0.0].CGColor;
    CGColorRef gradientStop = [UIColor colorWithWhite:0 alpha:0.3].CGColor;
    CGGradientRef gradient = CGGradientCreateWithColors(cs, (__bridge CFArrayRef)@[(__bridge id)gradientStart, (__bridge id)gradientStop], nil);
    CGPoint center = CGPointMake(bounds.size.width/2, bounds.size.height/2);
    CGContextDrawRadialGradient(ctx, gradient, center, bounds.size.width/2.5, center, bounds.size.width, 0);
    CGColorSpaceRelease(cs);
    CGGradientRelease(gradient);

    CGContextSetFillColorWithColor(ctx, [UIColor blackColor].CGColor);
    CGRect bottom = CGRectMake(-bounds.size.width, bounds.size.height-bounds.size.height*0.20, bounds.size.width*3, bounds.size.height/2);
    CGContextFillEllipseInRect(ctx, bottom);
    
    NSDictionary* attributes = @{
                                 NSFontAttributeName:               [UIFont fontWithName:@"HelveticaNeue-Thin" size:120],
                                 NSForegroundColorAttributeName:    [UIColor blackColor],
                                 NSBackgroundColorAttributeName:    [UIColor clearColor]
                                 };
    [[[NSAttributedString alloc] initWithString:title attributes:attributes] drawAtPoint:CGPointMake(20,10)];
    if (artist && artist.length>0) {
        attributes = @{
                       NSFontAttributeName:               [UIFont fontWithName:@"HelveticaNeue-Thin" size:90],
                       NSForegroundColorAttributeName:    [UIColor blackColor],
                       NSBackgroundColorAttributeName:    [UIColor clearColor]
                       };
        [[[NSAttributedString alloc] initWithString:artist attributes:attributes] drawAtPoint:CGPointMake(20,150)];
    }
    
    attributes = @{
                   NSFontAttributeName:               [UIFont fontWithName:@"HelveticaNeue-Bold" size:80],
                   NSForegroundColorAttributeName:    [UIColor whiteColor],
                   NSBackgroundColorAttributeName:    [UIColor clearColor]
                   };
    [[[NSAttributedString alloc] initWithString:format attributes:attributes] drawAtPoint:CGPointMake(20,bounds.size.height-110)];

    NSMutableParagraphStyle* ps = [[NSMutableParagraphStyle alloc] init];
    ps.alignment = NSTextAlignmentRight;

    attributes = @{
                   NSFontAttributeName:               [UIFont fontWithName:@"HelveticaNeue-Bold" size:80],
                   NSForegroundColorAttributeName:    [UIColor whiteColor],
                   NSBackgroundColorAttributeName:    [UIColor clearColor],
                   NSParagraphStyleAttributeName:     ps
                   };
    [[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%ldch", (long)nbrChannels] attributes:attributes] drawInRect:CGRectMake(bounds.size.width-220,bounds.size.height-110, 200, 110)];

    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    albumArts[hash] = image;

    return image;
}

+ (NSString*) removeHTMLinString:(NSString*)string {
    NSAttributedString* html = [[NSAttributedString alloc] initWithData:[string dataUsingEncoding:NSUTF8StringEncoding
                                                                             allowLossyConversion:YES]
                                                                options:@{
                                                                          NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType}
                                                     documentAttributes:nil
                                                                  error:nil];
    return [[html.string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
}

+ (NSArray*) sortModulesByTitle:(NSArray*)modules {
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"title_sort" ascending:YES];
    return [modules sortedArrayUsingDescriptors:@[descriptor]];
}

+ (NSArray*) sortArtistsByName:(NSArray *)artists {
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"name_sort" ascending:YES];
    return [artists sortedArrayUsingDescriptors:@[descriptor]];
}

+ (void)generateArtistNames:(NSMutableArray *)artists {
    for (NSMutableDictionary* artist in artists) {
        NSString* name = [self nameStringForArtist:artist];
        artist[@"name"] = name;
        artist[@"name_sort"] = [[[name componentsSeparatedByCharactersInSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]] componentsJoinedByString:@""] lowercaseString];
    }
}

+ (void)generateModuleTitles:(NSMutableArray *)modules {
    for (NSMutableDictionary* module in modules) {
        NSString* title = [self titleStringForModule:module];
        module[@"title"] = title;
        module[@"title_sort"] = [[[title componentsSeparatedByCharactersInSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]] componentsJoinedByString:@""] lowercaseString];
    }
}

@end
