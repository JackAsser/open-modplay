//
//  DonateTableViewController.h
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-24.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@interface DonateTableViewController : UITableViewController<UITableViewDelegate, UITableViewDataSource,SKProductsRequestDelegate>

@end
