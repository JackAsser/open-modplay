//
//  NSData+MD5.h
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-19.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData(MD5)

- (NSString *)MD5;

@end
