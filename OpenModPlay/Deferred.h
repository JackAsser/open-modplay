//
//  Deferred.h
//  objc-promise
//
//  Created by Michael Roberts on 2012-10-12.
//  Copyright (c) 2012 Mike Roberts. All rights reserved.
//

#import "Promise.h"

@interface Deferred<__covariant ObjectType> : Promise

+ (Deferred<ObjectType>*)deferred;

- (Promise<ObjectType>*)promise;
- (Promise<ObjectType>*)resolve:(id)result;
- (Promise<ObjectType>*)reject:(NSError *)reason;

@end
