//
//  SearchViewController.m
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-13.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchResultsTableViewController.h"

@implementation SearchViewController {
    UISearchController* searchController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SearchResultsTableViewController* searchResulsts = [self.storyboard instantiateViewControllerWithIdentifier:@"SEARCHRESULTS"];
    
    searchController = [[UISearchController alloc] initWithSearchResultsController:searchResulsts];
    searchController.searchResultsUpdater = searchResulsts;
    searchController.delegate = self;
    searchController.searchBar.delegate = self;
    searchController.hidesNavigationBarDuringPresentation = NO;
    searchController.dimsBackgroundDuringPresentation = YES;
    searchController.definesPresentationContext = YES;
    searchController.searchBar.placeholder = @"Search songs or artists";
    searchController.searchBar.barStyle = UIBarStyleDefault;
    searchController.searchBar.keyboardAppearance = UIKeyboardAppearanceDark;
    self.navigationItem.titleView = searchController.searchBar;
    self.definesPresentationContext = YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];

    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.text = @"test";
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    return cell;
}

@end
