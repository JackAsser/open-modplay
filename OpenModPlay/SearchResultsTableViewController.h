//
//  SearchResultsTableViewController.h
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-13.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultsTableViewController : UITableViewController<UISearchResultsUpdating>

@end
