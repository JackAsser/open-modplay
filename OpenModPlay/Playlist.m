//
//  Playlist.m
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-19.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import "Playlist.h"
#import "Database.h"

@implementation Playlist {
    CKRecord* _record;
}

-(instancetype)initWithName:(NSString *)name {
    self = [super init];
    _name = name;
    _list = [NSMutableArray array];
    _record = [[CKRecord alloc] initWithRecordType:@"Playlist"];
    _record[@"name"] = name;
    return self;
}

-(instancetype)initFromCloudPlaylist:(CKRecord *)playlist {
    _record = playlist;
    _name = playlist[@"name"];
    _list = [NSMutableArray array];
    NSArray<CKReference*>* list = playlist[@"items"];
    for (CKReference* reference in list) {
        CKRecord* module = [[Database sharedInstance] findModuleByRecordID:reference.recordID];
        if (module)
            [_list addObject:module];
    }
    
    return self;
}

-(CKRecord*)record {
    NSMutableArray<CKReference*>* list = [NSMutableArray array];

    for (CKRecord* module in _list) {
        CKReference* reference = [[CKReference alloc] initWithRecord:module action:CKReferenceActionNone];
        [list addObject:reference];
    }
    _record[@"items"] = list;

    return _record;
}

@end
