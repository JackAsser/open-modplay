//
//  MainViewController.m
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-11.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import "MainViewController.h"
#import "Playback.h"
#import "Utilities.h"
#import <MediaPlayer/MediaPlayer.h>

@implementation MainViewController {
    NSMutableDictionary *songInfo;
}

- (void)updateProgress {
    openmpt_module* module = [Playback sharedInstance].currentModule;
    
    if (module) {
        double duration = openmpt_module_get_duration_seconds(module);
        double position = openmpt_module_get_position_seconds(module);
        [songInfo setObject:@(duration) forKey:MPMediaItemPropertyPlaybackDuration];
        [songInfo setObject:@(position) forKey:MPNowPlayingInfoPropertyElapsedPlaybackTime];
        [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:songInfo];
    }
}

- (void)updateSongInfo {
    CKRecord* moduleRecord = [Playback sharedInstance].currentModuleRecord;
    openmpt_module* module = [Playback sharedInstance].currentModule;

    if (module && moduleRecord) {
        MPMediaItemArtwork *albumArt = [[MPMediaItemArtwork alloc] initWithImage:[Utilities albumArtForModule:module record:moduleRecord]];
        NSLog(@"Setting albumArt %@ to current artwork", albumArt);
        [songInfo setObject:albumArt forKey:MPMediaItemPropertyArtwork];
        [songInfo setObject:@(MPMediaTypeMusic) forKey:MPMediaItemPropertyMediaType];
        [songInfo setObject:moduleRecord[@"title"] forKey:MPMediaItemPropertyTitle];
        [songInfo setObject:[Utilities artistStringForModuleRecord:moduleRecord] forKey:MPMediaItemPropertyArtist];
        [songInfo setObject:@(0) forKey:MPMediaItemPropertyPlaybackDuration];
        [songInfo setObject:@(0) forKey:MPNowPlayingInfoPropertyElapsedPlaybackTime];
        [songInfo setObject:@([Playback sharedInstance].playlistPosition) forKey:MPNowPlayingInfoPropertyPlaybackQueueIndex];
        [songInfo setObject:@([Playback sharedInstance].playlist.list.count) forKey:MPNowPlayingInfoPropertyPlaybackQueueCount];
        
        [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:songInfo];
    }
    else
        [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:nil];
}

- (void)observeSongInfo {
    songInfo = [[NSMutableDictionary alloc] init];

    [[NSNotificationCenter defaultCenter] addObserverForName:PlaybackProgressNotification object:[Playback sharedInstance] queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        [self updateProgress];
    }];

    [[NSNotificationCenter defaultCenter] addObserverForName:PlaybackNewModuleNotification object:[Playback sharedInstance] queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        [self updateSongInfo];
    }];    
}

- (void)handleRemoteControlEvents {
    [[MPRemoteCommandCenter sharedCommandCenter].pauseCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent * _Nonnull event) {
        if ([Playback sharedInstance].currentModule == nil)
            return MPRemoteCommandHandlerStatusNoActionableNowPlayingItem;
        [[Playback sharedInstance] pause];
        return MPRemoteCommandHandlerStatusSuccess;
    }];

    [[MPRemoteCommandCenter sharedCommandCenter].stopCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent * _Nonnull event) {
        if ([Playback sharedInstance].currentModule == nil)
            return MPRemoteCommandHandlerStatusNoActionableNowPlayingItem;
        [[Playback sharedInstance] stop];
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    
    [[MPRemoteCommandCenter sharedCommandCenter].playCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent * _Nonnull event) {
        if ([Playback sharedInstance].currentModule == nil)
            return MPRemoteCommandHandlerStatusNoActionableNowPlayingItem;
        [[Playback sharedInstance] play];
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    
    [[MPRemoteCommandCenter sharedCommandCenter].togglePlayPauseCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent * _Nonnull event) {
        if ([Playback sharedInstance].currentModule == nil)
            return MPRemoteCommandHandlerStatusNoActionableNowPlayingItem;
        [[Playback sharedInstance] togglePlay];        
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    
    [[MPRemoteCommandCenter sharedCommandCenter].nextTrackCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent * _Nonnull event) {
        [[Playback sharedInstance] next];
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    
    [[MPRemoteCommandCenter sharedCommandCenter].previousTrackCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent * _Nonnull event) {
        [[Playback sharedInstance] previous];
        return MPRemoteCommandHandlerStatusSuccess;
    }];
}

- (void)createPopup {
    UIViewController* popup = [self.storyboard instantiateViewControllerWithIdentifier:@"POPUP"];
    [self.view insertSubview:popup.view belowSubview:self.tabBar];
    [self addChildViewController:popup];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self createPopup];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self observeSongInfo];
    [self handleRemoteControlEvents];
}

@end
