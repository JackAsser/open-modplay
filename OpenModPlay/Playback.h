//
//  Playback.h
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-11.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <libopenmpt/libopenmpt.h>
#import "Playlist.h"

extern NSNotificationName _Nonnull PlaybackProgressNotification;
extern NSNotificationName _Nonnull PlaybackNewModuleNotification;
extern NSNotificationName _Nonnull PlaybackStateChangedNotification;

@interface Playback : NSObject

@property (readonly, nullable) openmpt_module* currentModule;
@property (readonly, nullable) CKRecord* currentModuleRecord;
@property (readwrite, nullable, nonatomic) Playlist* playlist;
@property (readwrite, nonatomic) NSInteger playlistPosition;
@property (readonly) double sampleRate;
@property (readonly) BOOL playing;
@property (readwrite) BOOL autoNextEnabled;

+ (Playback* _Nonnull)sharedInstance;

- (void)setPlaylist:(Playlist* _Nonnull)playlist position:(NSInteger)playlistPosition;

- (void)pause;
- (void)play;
- (void)togglePlay;
- (void)stop;
- (void)next;
- (void)previous;
- (void)previousForce;

@end
