//
//  XMLToDictionary.h
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-13.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMLToDictionary : NSObject<NSXMLParserDelegate>

@property(readonly, nonatomic) NSDictionary* dictionary;

@end
