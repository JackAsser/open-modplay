//
//  AlertAction.h
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-15.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertAction : NSObject

@property (readonly, nullable) UIImage* image;
@property (readonly, nullable) NSString* title;

+ (instancetype _Nonnull )actionWithTitle:(nullable NSString *)title image:(nullable UIImage*)image handler:(void (^ __nullable)(AlertAction * _Nullable action))handler;

@end
