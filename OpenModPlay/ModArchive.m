//
//  ModArchive.m
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-13.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import "ModArchive.h"
#import "modarchive.key.h"
#import "XMLToDictionary.h"
#import "Deferred.h"

#ifndef QUERY_ENDPOINT
#   define ENDPOINT @"https://api.modarchive.org/xml-tools.php"
#endif

#ifndef DOWNLOAD_ENDPOINT
#   define DOWNLOAD_ENDPOINT @"https://api.modarchive.org/downloads.php"
#endif

@implementation ModArchive {
    
}

+ (id)sharedInstance {
    static id shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

- (NSString*) urlEncodedString:(NSString*)string {
    NSMutableString * output = [NSMutableString string];
    const unsigned char * source = (const unsigned char *)[string UTF8String];
    unsigned long sourceLen = strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

- (NSArray*)parametersToQueryItems:(NSDictionary*)parameters {
    NSMutableArray* tmp = [NSMutableArray array];
    for (NSString* key in parameters) {
        NSString* value = [NSString stringWithFormat:@"%@", parameters[key]];
        value = [self urlEncodedString:value];
        NSURLQueryItem* item = [NSURLQueryItem queryItemWithName:key value:value];
        [tmp addObject:item];
    }
    return tmp;
}

- (NSURL*)generateQueryURL:(NSDictionary*)query {
    NSMutableDictionary* tmp = [NSMutableDictionary dictionaryWithDictionary:@{@"key": MODARCHIVE_APIKEY}];
    [tmp addEntriesFromDictionary:query];
    
    NSURLComponents* url = [[NSURLComponents alloc] initWithString:QUERY_ENDPOINT];
    url.queryItems = [self parametersToQueryItems:tmp];
    return url.URL;
}

- (NSURL*)generateDownloadURL:(NSDictionary*)query {
    NSURLComponents* url = [[NSURLComponents alloc] initWithString:DOWNLOAD_ENDPOINT];
    url.queryItems = [self parametersToQueryItems:query];
    return url.URL;
}

- (Promise*)query:(NSDictionary*)query {
    Deferred* deferred = [Deferred deferred];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL* url = [self generateQueryURL:query];
        //NSLog(@"%@", url);
        NSXMLParser* parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
        XMLToDictionary* xmlToDictionary = [[XMLToDictionary alloc] init];
        parser.delegate = xmlToDictionary;
        if ([parser parse])
            [deferred resolve:xmlToDictionary.dictionary];
        else
            [deferred reject:nil];
    });

    return deferred.promise;
}

- (Promise*)download:(NSDictionary*)query {
    Deferred* deferred = [Deferred deferred];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL* url = [self generateDownloadURL:query];

        NSURLSessionConfiguration* configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession* session = [NSURLSession sessionWithConfiguration:configuration];
        NSURLSessionDownloadTask* task = [session downloadTaskWithURL:url
                                                    completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                                        if (error) {
                                                            [deferred reject:error];
                                                            return;
                                                        }
                                                        NSHTTPURLResponse* r = (NSHTTPURLResponse*)response;
                                                        if (r.statusCode != 200) {
                                                            [deferred reject:nil];
                                                            return;
                                                        }
                                                        [deferred resolve:location];
                                                    }];
        [task resume];
    });
    
    return deferred.promise;
}

- (Promise*)searchForModule:(NSString *)searchString page:(NSUInteger)page {
    return [self query:@{@"request": @"search",
                         @"type": @"filename_or_songtitle",
                         @"page": @(page),
                         @"query": [NSString stringWithFormat:@"*%@*", searchString]
                        }
            ];
}

- (Promise*)searchForArtist:(NSString *)searchString page:(NSUInteger)page {
    return [self query:@{@"request": @"search_artist",
                         @"page": @(page),
                         @"query": [NSString stringWithFormat:@"%@", searchString]
                         }
            ];
}

- (Promise*)modulesByArtist:(NSString *)artistID page:(NSUInteger)page {
    return [self query:@{@"request": @"view_modules_by_artistid",
                         @"page": @(page),
                         @"query": artistID
                         }
            ];
}

- (Promise*)getModuleByHash:(NSString*)hash {
    return [self query:@{@"request": @"search",
                         @"type": @"hash",
                         @"query": hash
                         }
            ];
}


- (Promise*)downloadModuleById:(NSString *)_id {
    return [self download:@{@"moduleid": _id}];
}

@end
