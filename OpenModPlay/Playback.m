//
//  Playback.m
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-11.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import "Playback.h"
#import <AudioUnit/AudioUnit.h>
#import <AVFoundation/AVFoundation.h>
#import "Database.h"

@interface Playback()
- (void)fireProgress;
- (void)freeCurrentModule;
@end

#define kOutputBus 0
#define kInputBus 1

NSNotificationName PlaybackProgressNotification = @"PlaybackProgressNotification";
NSNotificationName PlaybackNewModuleNotification = @"PlaybackNewModuleNotification";
NSNotificationName PlaybackStateChangedNotification = @"PlaybackStateChangedNotification";

static OSStatus playbackCallback(void *inRefCon,
                                 AudioUnitRenderActionFlags *ioActionFlags,
                                 const AudioTimeStamp *inTimeStamp,
                                 UInt32 inBusNumber,
                                 UInt32 inNumberFrames,
                                 AudioBufferList *ioData) {
    Playback* playback = [Playback sharedInstance];
    if ((!playback.playing) || (!playback.currentModule))
        return noErr;

    for (int i=0; i<ioData->mNumberBuffers; i++) {
        short* buf = (short*)ioData->mBuffers[i].mData;
        unsigned int count = (ioData->mBuffers[i].mDataByteSize) / 4;
        size_t written = openmpt_module_read_interleaved_stereo(playback.currentModule, (int)playback.sampleRate, count, buf);
        if ((written == 0) && playback.autoNextEnabled) {
            [playback freeCurrentModule];
            [playback next];
            break;
        }
    }

    [playback performSelectorInBackground:@selector(fireProgress) withObject:nil];

    return noErr;
}


@implementation Playback {
    AVAudioSession* _session;
    AudioComponentInstance _audioUnit;
    double _lastPosition;
    BOOL _interrupted;
    openmpt_module* _playingModule;
}

+ (id)sharedInstance {
    static id shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

- (void)freeCurrentModule {
    if (_currentModule) {
        openmpt_module_destroy(_currentModule);
        _currentModule = nil;
    }
}

- (void)fireProgress {
    if (_playingModule != _currentModule) {
        _playingModule = _currentModule;
        [[NSNotificationCenter defaultCenter] postNotificationName:PlaybackNewModuleNotification object:self];
    }
    
    if (![Playback sharedInstance].currentModule)
        return;
    
    double position = openmpt_module_get_position_seconds([Playback sharedInstance].currentModule);
    if (fabs(position - _lastPosition) > 0.1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:PlaybackProgressNotification object:self];
        _lastPosition = position;
    }
}

- (void)loadModule {
    CKRecord* moduleRecord = _playlist.list[_playlistPosition];
    [[[Database sharedInstance] getDataFromModule:moduleRecord
                                     addToLibrary:NO] when:^(NSData* data) {
        openmpt_module* newModule = openmpt_module_create_from_memory((void*)data.bytes, data.length, NULL, NULL, NULL);

        if (!newModule)
            NSLog(@"Failed to open mod '%@'", moduleRecord[@"title"]);
        else
            NSLog(@"Mod successfully opened '%@'", moduleRecord[@"title"]);
        
        openmpt_module_set_position_seconds(newModule, 0);
        openmpt_module_set_render_param(newModule, OPENMPT_MODULE_RENDER_STEREOSEPARATION_PERCENT, 0);
        _lastPosition = -100;

        if (_currentModule) {
            openmpt_module* oldModule = _currentModule;
            _currentModule = nil;
            openmpt_module_destroy(oldModule);
        }
        _currentModule = newModule;
        _currentModuleRecord = moduleRecord;
    } failed:^(NSError *error) {
        NSLog(@"%@", error);
    }];
}

- (id)init {
    self = [super init];
    
    _sampleRate = 48000;
    _autoNextEnabled = YES;
    _interrupted = NO;
    
    NSError *error = nil;
    _session = [AVAudioSession sharedInstance];
    [_session setPreferredSampleRate:_sampleRate error:&error];
    [_session setPreferredIOBufferDuration:0.2 error:&error];
    [_session setCategory:AVAudioSessionCategoryPlayback error:&error];
    _sampleRate = _session.sampleRate;

    [[NSNotificationCenter defaultCenter] addObserverForName:AVAudioSessionInterruptionNotification object:_session queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        AVAudioSessionInterruptionType interruptionType = [note.userInfo[AVAudioSessionInterruptionTypeKey] intValue];
        switch (interruptionType) {
            case AVAudioSessionInterruptionTypeBegan: {
                if (_playing) {
                    [self pause];
                    _interrupted = YES;
                }
                else
                    _interrupted = NO;
                break;
            }
            case AVAudioSessionInterruptionTypeEnded: {
                AVAudioSessionInterruptionOptions interruptionOptions = [note.userInfo[AVAudioSessionInterruptionOptionKey] intValue];
                if ((interruptionOptions == AVAudioSessionInterruptionOptionShouldResume) && _interrupted)
                    [self play];
                _interrupted = NO;
                break;
            }
        }
    }];

    OSStatus status;
    
    // Describe audio component
    AudioComponentDescription desc;
    desc.componentType = kAudioUnitType_Output;
    desc.componentSubType = kAudioUnitSubType_RemoteIO;
    desc.componentFlags = 0;
    desc.componentFlagsMask = 0;
    desc.componentManufacturer = kAudioUnitManufacturer_Apple;
    
    // Get component
    AudioComponent inputComponent = AudioComponentFindNext(NULL, &desc);
    
    // Get audio units
    status = AudioComponentInstanceNew(inputComponent, &_audioUnit);
    //checkStatus(status);
    
    UInt32 flag = 1;
    // Enable IO for playback
    status = AudioUnitSetProperty(_audioUnit,
                                  kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Output,
                                  kOutputBus,
                                  &flag,
                                  sizeof(flag));
    //checkStatus(status);
    
    // Describe format
    AudioStreamBasicDescription audioFormat;
    audioFormat.mSampleRate = _sampleRate;
    audioFormat.mFormatID = kAudioFormatLinearPCM;
    audioFormat.mFormatFlags = kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
    audioFormat.mFramesPerPacket = 1;
    audioFormat.mChannelsPerFrame = 2;
    audioFormat.mBitsPerChannel = 16;
    audioFormat.mBytesPerPacket = 4;
    audioFormat.mBytesPerFrame = 4;
    
    // Apply format
    
    status = AudioUnitSetProperty(_audioUnit,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Input,
                                  kOutputBus,
                                  &audioFormat,
                                  sizeof(audioFormat));
    //  checkStatus(status);
    
    // Set output callback
    AURenderCallbackStruct callbackStruct;
    callbackStruct.inputProc = playbackCallback;
    callbackStruct.inputProcRefCon = (__bridge void*)self;
    status = AudioUnitSetProperty(_audioUnit,
                                  kAudioUnitProperty_SetRenderCallback,
                                  kAudioUnitScope_Global,
                                  kOutputBus,
                                  &callbackStruct,
                                  sizeof(callbackStruct));
    
    // Initialize
    status = AudioUnitInitialize(_audioUnit);
    
    return self;
}

- (void)play {
    if (_playing)
        return;
    if (!_currentModule)
        return;
    _playing = YES;
    [_session setActive:YES error:nil];
    AudioOutputUnitStart(_audioUnit);
    NSLog(@"Play");
    [[NSNotificationCenter defaultCenter] postNotificationName:PlaybackStateChangedNotification object:self];
}

- (void)stop {
    if (!_playing)
        return;
    _playing = NO;
    AudioOutputUnitStop(_audioUnit);
    if (_currentModule)
        openmpt_module_set_position_seconds(_currentModule, 0);
    NSLog(@"Stop");
    [[NSNotificationCenter defaultCenter] postNotificationName:PlaybackStateChangedNotification object:self];
}

- (void)pause {
    if (!_playing)
        return;
    _playing = NO;
    AudioOutputUnitStop(_audioUnit);
    NSLog(@"Pause");
    [[NSNotificationCenter defaultCenter] postNotificationName:PlaybackStateChangedNotification object:self];
}

- (void)togglePlay {
    if (_playing)
        [self pause];
    else
        [self play];
}

- (void)setPlaylist:(Playlist*)playlist {
    if (playlist != _playlist) {
        _playlist = playlist;
        _playlistPosition = 0;
        [self loadModule];
    }
}

- (void)setPlaylistPosition:(NSInteger)playlistPosition {
    while (playlistPosition<0)
        playlistPosition += _playlist.list.count;
    playlistPosition %= _playlist.list.count;

    if (_playlistPosition != playlistPosition) {
        _playlistPosition = playlistPosition;
        [self loadModule];
    }
}

- (void)setPlaylist:(Playlist*)playlist position:(NSInteger)playlistPosition {
    while (playlistPosition<0)
        playlistPosition += _playlist.list.count;
    playlistPosition %= _playlist.list.count;
    
    if ((playlist != _playlist) || (playlistPosition != _playlistPosition)) {
        _playlist = playlist;
        _playlistPosition = playlistPosition;
        [self loadModule];
    }
}

- (void)next {
    if (!_playlist)
        return;
    
    _playlistPosition++;
    _playlistPosition %= _playlist.list.count;
    [self loadModule];
}

- (void)previousForce {
    if (!_playlist)
        return;
    
    _playlistPosition--;
    while (_playlistPosition<0)
        _playlistPosition += _playlist.list.count;
    _playlistPosition %= _playlist.list.count;
    [self loadModule];
}

- (void)previous {
    if (!_playlist)
        return;

    if (_currentModule) {
        if (openmpt_module_get_position_seconds(_currentModule) > 5) {
            openmpt_module_set_position_seconds(_currentModule, 0);
            return;
        }
    }
    
    [self previousForce];
}

@end
