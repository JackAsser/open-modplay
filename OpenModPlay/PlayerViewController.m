//
//  PlayerViewController.m
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-12.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import "Playback.h"
#import "PlayerViewController.h"
#import "PopupViewController.h"
#import "ActionSheet.h"
#import <MediaPlayer/MediaPlayer.h>
#import "Utilities.h"

@implementation PlayerViewController {
    IBOutlet UIButton* airplayAvailableButton;
    IBOutlet UIButton* playPauseButton;
    IBOutlet UISlider* scrubbSlider;
    IBOutlet UIImageView* backgroundImageView;
    IBOutlet UIImageView* songImageView;
    BOOL userIsScrubbing;
    BOOL wirelessRoutesAvailable;
    MPVolumeView* volumeView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    wirelessRoutesAvailable = NO;
    userIsScrubbing = NO;
    
    scrubbSlider.value = 0;
    [[NSNotificationCenter defaultCenter] addObserverForName:PlaybackProgressNotification object:[Playback sharedInstance] queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        if (userIsScrubbing)
            return;
        double duration = openmpt_module_get_duration_seconds([Playback sharedInstance].currentModule);
        double position = openmpt_module_get_position_seconds([Playback sharedInstance].currentModule);
        scrubbSlider.value = (float)(position / duration);
    }];

    [[NSNotificationCenter defaultCenter] addObserverForName:PlaybackStateChangedNotification object:[Playback sharedInstance] queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        [self updateUI];
    }];

    [[NSNotificationCenter defaultCenter] addObserverForName:PlaybackNewModuleNotification object:[Playback sharedInstance] queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        [self updateUI];
    }];

    volumeView = [[MPVolumeView alloc] init];
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(checkWirelessRoutes) userInfo:nil repeats:YES];
    [self updateUI];
}

- (void)checkWirelessRoutes {
    if (volumeView.wirelessRoutesAvailable != wirelessRoutesAvailable) {
        wirelessRoutesAvailable = volumeView.wirelessRoutesAvailable;
        [self performSelectorOnMainThread:@selector(updateUI) withObject:nil waitUntilDone:NO];
    }
}

- (void)updateUI {
    UIImage* albumArt = [Utilities albumArtForModule:[Playback sharedInstance].currentModule
                                              record:[Playback sharedInstance].currentModuleRecord];
    backgroundImageView.image = albumArt;
    songImageView.image = albumArt;

    if ([Playback sharedInstance].playing)
        [playPauseButton setImage:[UIImage imageNamed:@"pause-big"] forState:UIControlStateNormal];
    else
        [playPauseButton setImage:[UIImage imageNamed:@"play-big"] forState:UIControlStateNormal];

    airplayAvailableButton.hidden = !wirelessRoutesAvailable;
}

- (IBAction)playPausePressed:(id)sender {
    [[Playback sharedInstance] togglePlay];
}

- (IBAction)previousPressed:(id)sender {
    [[Playback sharedInstance] previousForce];
}

- (IBAction)nextPressed:(id)sender {
    [[Playback sharedInstance] next];
}

- (IBAction)expandButtonPressed:(id)sender {
    [(PopupViewController*)self.parentViewController collapse];
}

- (IBAction)beginScrubbing {
    userIsScrubbing = YES;
    [Playback sharedInstance].autoNextEnabled = NO;
}

- (IBAction)endScrubbing {
    userIsScrubbing = NO;
    [Playback sharedInstance].autoNextEnabled = YES;
}

- (IBAction)scrubb {
    double duration = openmpt_module_get_duration_seconds([Playback sharedInstance].currentModule);
    openmpt_module_set_position_seconds([Playback sharedInstance].currentModule, duration * scrubbSlider.value);
}

- (IBAction)handleDrag:(UIPanGestureRecognizer *)recognizer {
    [(PopupViewController*)self.parentViewController handleDrag:recognizer];
}

- (IBAction)ellipsisPressed:(id)sender {
    ActionSheet* sheet = [self.storyboard instantiateViewControllerWithIdentifier:@"ACTIONSHEET"];
    sheet.title = @"Onward Ride";
    sheet.subTitle = @"Jugi";
    
    [sheet addAction:[AlertAction actionWithTitle:@"Save"
                                            image:[UIImage imageNamed:@"save"]
                                          handler:^(AlertAction * _Nullable action) {
                                          }]];

    [sheet addAction:[AlertAction actionWithTitle:@"Add to playlist"
                                            image:[UIImage imageNamed:@"playlist-add"]
                                          handler:^(AlertAction * _Nullable action) {
                                          }]];

    [sheet addAction:[AlertAction actionWithTitle:@"Add to queue"
                                            image:[UIImage imageNamed:@"queue-add"]
                                          handler:^(AlertAction * _Nullable action) {
                                          }]];

    [sheet addAction:[AlertAction actionWithTitle:@"Goto artist"
                                            image:[UIImage imageNamed:@"artist"]
                                          handler:^(AlertAction * _Nullable action) {
                                          }]];

    [self presentViewController:sheet animated:YES completion:nil];
}

@end
