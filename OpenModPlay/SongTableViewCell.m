//
//  SongTableViewCell.m
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-18.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import "SongTableViewCell.h"
#import "Utilities.h"
#import "ActionSheet.h"
#import "Playback.h"

@implementation SongTableViewCell

static UIImage* ellipsisImage;

+ (void)initialize {
    ellipsisImage = [[UIImage imageNamed:@"ellipsis"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

- (void)ellipsisTapped:(id)sender event:(id)event {
    ActionSheet* sheet = [_viewController.storyboard instantiateViewControllerWithIdentifier:@"ACTIONSHEET"];
    
    sheet.title = _module[@"title"];
    sheet.subTitle = [Utilities artistStringForModuleRecord:_module];
                                 
    [sheet addAction:[AlertAction actionWithTitle:@"Save"
                                            image:[UIImage imageNamed:@"save"]
                                          handler:^(AlertAction * _Nullable action) {
                                          }]];

    [sheet addAction:[AlertAction actionWithTitle:@"Add to playlist"
                                            image:[UIImage imageNamed:@"playlist-add"]
                                          handler:^(AlertAction * _Nullable action) {
                                          }]];
    
    [sheet addAction:[AlertAction actionWithTitle:@"Add to queue"
                                            image:[UIImage imageNamed:@"queue-add"]
                                          handler:^(AlertAction * _Nullable action) {
                                          }]];
    
    if (_currentArtistId == nil) {
        [sheet addAction:[AlertAction actionWithTitle:@"Goto artist"
                                                image:[UIImage imageNamed:@"artist"]
                                              handler:^(AlertAction * _Nullable action) {
                                              }]];
    }
    
    [_viewController presentViewController:sheet animated:YES completion:nil];
}

- (void)setModule:(CKRecord *)module {
    _module = module;
 
    self.textLabel.text = _module[@"title"];
    self.detailTextLabel.text = [Utilities artistStringForModuleRecord:_module];
    UIButton* accessory = [[UIButton alloc] initWithFrame:CGRectMake(0,0,44,44)];
    [accessory setImageEdgeInsets:UIEdgeInsetsMake(0,0,0,-35)];
    [accessory setImage:ellipsisImage forState:UIControlStateNormal];
    [accessory setTitle:@"" forState:UIControlStateNormal];
    accessory.tintColor = [UIColor lightGrayColor];
    [accessory addTarget:self action:@selector(ellipsisTapped:event:) forControlEvents:UIControlEventTouchUpInside];
    self.accessoryView = accessory;

    CKRecord* currentRecord = [Playback sharedInstance].currentModuleRecord;
    if ([Playback sharedInstance].playing && currentRecord && [currentRecord.recordID.recordName isEqualToString:module.recordID.recordName])
        self.textLabel.textColor = [UIColor orangeColor];
    else
        self.textLabel.textColor = [UIColor whiteColor];
    self.detailTextLabel.textColor = [UIColor lightGrayColor];
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
