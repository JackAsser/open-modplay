//
//  ArtistTableViewController.h
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-14.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtistTableViewController : UITableViewController

@property(readwrite) NSString* artistID;
@property(readwrite) NSString* name;

@end
