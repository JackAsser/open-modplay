//
//  SongTableViewCell.h
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-18.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Database.h"

@interface SongTableViewCell : UITableViewCell

@property (readwrite) NSString* currentArtistId;
@property (readwrite) NSString* currentPlaylistId;
@property (readwrite) UIViewController* viewController;
@property (readwrite, nonatomic) CKRecord* module;

@end
