//
//  Database.m
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-15.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import "Database.h"
#import "Deferred.h"
#import "Utilities.h"
#import "ModArchive.h"
#import "NSData+MD5.h"

/*

* Database stored as json in a file:
    local
        artists[]
        modules[]
        playlists[]
    pending-add
        artists[]
        modules[]
        playlists[]
    pending-delete
        artistsIDs[]
        moduleIDs[]
        playlistIDs[]
    subscriptions[] Artist,Module,Playlist

* On start / connectivity change:
    Create?
        Download remote and create local database
    else
        Sync pending adds/deletes
 
    Subscribe to artists, modules and playlists if not already subscribed

* On add:
    Add locally and add to pending-add
    Sync pending adds/deletes
 
* On delete:
    Delete locally and add to pending-delete
    Sync pending adds/deletes
 
* On modify:
    Modify locally and

* On notification add:
    Add locally
 
* On notification delete:
    Delete locally
 
* On notification modify:
    Modifiy locally
 
*/

@implementation Database {
    Deferred* _readyDeferred;

    NSMutableArray* _artists;
    NSMutableArray* _modules;
    NSMutableArray* _playlists;

    NSMutableArray* _artists_pending_add;
    NSMutableArray* _modules_pending_add;
    NSMutableArray* _playlists_pending_add;

    NSMutableArray* _artistsIDs_pending_delete;
    NSMutableArray* _modulesIDs_pending_delete;
    NSMutableArray* _playlistsIDs_pending_delete;
    
    NSMutableArray* _cachedArtists;
    NSMutableArray* _cachedModules;
}

+ (instancetype)sharedInstance {
    static id shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

- (NSArray*)artists {
    return [_artists copy];
}

- (NSArray*)modules {
    return [_modules copy];
}

- (NSArray*)playlists {
    return [_playlists copy];
}

- (Promise*)ready {
    return _readyDeferred.promise;
}

#define CLOUDKIT_REQUEST_TIMEOUT 30
#define CLOUDKIT_RESOURCE_TIMEOUT 30

- (void)fetchRecordsWithType:(NSString *)recordType predicate:(NSPredicate*)predicate fullRecords:(BOOL)full completionHandler:(void (^)(NSArray *records, NSError *error))completionHandler {
    
    CKQuery *query = [[CKQuery alloc] initWithRecordType:recordType
                                               predicate:predicate];
    
    CKQueryOperation *queryOperation = [[CKQueryOperation alloc] initWithQuery:query];
    if (!full)
        queryOperation.desiredKeys = @[];
    NSMutableArray *results = [NSMutableArray new];
  
//  iOS 10 specific
//    queryOperation.timeoutIntervalForRequest = CLOUDKIT_REQUEST_TIMEOUT;
//    queryOperation.timeoutIntervalForResource = CLOUDKIT_RESOURCE_TIMEOUT;
    
    queryOperation.recordFetchedBlock = ^(CKRecord *record) {
        [results addObject:record];
    };

    queryOperation.queryCompletionBlock = ^(CKQueryCursor *cursor, NSError *error) {
        [self retrieveNextBatchOfQueryFromCursor:cursor
                                         results:results
                                           error:error
                               completionHandler:completionHandler];
    };
    
    CKDatabase* database = [CKContainer defaultContainer].privateCloudDatabase;
    [database addOperation:queryOperation];
}

- (void)retrieveNextBatchOfQueryFromCursor:(CKQueryCursor *)cursor
                                   results:(NSMutableArray *)results
                                     error:(NSError *)error
                         completionHandler:(void (^)(NSArray *records, NSError *error))completionHandler {
    if (cursor != nil && !error) {
        CKQueryOperation *nextOperation = [[CKQueryOperation alloc] initWithCursor:cursor];
        
        nextOperation.timeoutIntervalForRequest = CLOUDKIT_REQUEST_TIMEOUT;
        nextOperation.timeoutIntervalForResource = CLOUDKIT_RESOURCE_TIMEOUT;

        nextOperation.recordFetchedBlock = ^(CKRecord *record) {
            [results addObject:record];
        };
    
        nextOperation.queryCompletionBlock = ^(CKQueryCursor *cursor, NSError *error) {
            [self retrieveNextBatchOfQueryFromCursor:cursor
                                             results:results
                                               error:error
                                   completionHandler:completionHandler];
        };
        
        CKDatabase* database = [CKContainer defaultContainer].privateCloudDatabase;
        [database addOperation:nextOperation];
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            completionHandler(results, error);
        });
    }
}

- (Promise*)fetchRecordsOfType:(NSString*)type usingPredicate:(NSPredicate*)predicate fullRecords:(BOOL)fullRecords {
    Deferred* deferred = [Deferred deferred];

    [self fetchRecordsWithType:type predicate:predicate fullRecords:fullRecords completionHandler:^(NSArray *records, NSError *error) {
        if (error)
            [deferred reject:error];
        else {
            [deferred resolve:records];
        }
    }];

    return deferred.promise;
}

- (BOOL)modArchiveModuleExists:(NSDictionary*)moduleInfo {
    return [self findModuleByHash:moduleInfo[@"hash"]] != nil;
}

- (CKRecord*)findModuleByRecordID:(CKRecordID*)recordID {
    NSPredicate* filter = [NSPredicate predicateWithFormat:@"recordID = %@", recordID];
    CKRecord* record = [[_modules filteredArrayUsingPredicate:filter] firstObject];
    if (record)
        return record;
    return [[_cachedModules filteredArrayUsingPredicate:filter] firstObject];
}

- (CKRecord*)findModuleByHash:(NSString*)hash {
    NSPredicate* filter = [NSPredicate predicateWithFormat:@"hash = %@", hash];
    return [[_modules filteredArrayUsingPredicate:filter] firstObject];
}

- (CKRecord*)findCachedModuleByHash:(NSString*)hash {
    NSPredicate* filter = [NSPredicate predicateWithFormat:@"hash = %@", hash];
    return [[_cachedModules filteredArrayUsingPredicate:filter] firstObject];
}

- (CKRecord*)findArtistByRecordID:(CKRecordID*)recordID {
    NSPredicate* filter = [NSPredicate predicateWithFormat:@"recordID = %@", recordID];
    CKRecord* record = [[_artists filteredArrayUsingPredicate:filter] firstObject];
    if (record)
        return record;
    return [[_cachedArtists filteredArrayUsingPredicate:filter] firstObject];
}

- (CKRecord*)findArtistById:(NSString*)_id {
    NSPredicate* filter = [NSPredicate predicateWithFormat:@"id = %@", _id];
    return [[_artists filteredArrayUsingPredicate:filter] firstObject];
}

- (CKRecord*)findArtistByAlias:(NSString*)alias {
    NSPredicate* filter = [NSPredicate predicateWithFormat:@"alias = %@", alias];
    return [[_artists filteredArrayUsingPredicate:filter] firstObject];
}

- (CKRecord*)findCachedArtistById:(NSString*)_id {
    NSPredicate* filter = [NSPredicate predicateWithFormat:@"id = %@", _id];
    return [[_cachedArtists filteredArrayUsingPredicate:filter] firstObject];
}

- (CKRecord*)findCachedArtistByAlias:(NSString*)alias {
    NSPredicate* filter = [NSPredicate predicateWithFormat:@"alias = %@", alias];
    return [[_cachedArtists filteredArrayUsingPredicate:filter] firstObject];
}

- (CKRecord*)importArtistFromModArchive:(NSDictionary*)artistInfo addToLibrary:(BOOL)addToLibrary {
    NSString* name = artistInfo[@"name"];
    NSString* _id = artistInfo[@"id"];
    NSString* alias = [name lowercaseString];

    CKRecord* artist = _id ? [self findArtistById:_id] : [self findArtistByAlias:alias];
    if (artist) {
        // Already in library, but library version didn't have ID, but now we do! Update!
        if (!artist[@"id"] && _id) {
            artist[@"id"] = _id;
            [_artists_pending_add addObject:artist];
        }
        return artist;
    }

    artist = _id ? [self findCachedArtistById:_id] : [self findCachedArtistByAlias:alias];
    if (artist) {
        if (addToLibrary) {
            [_artists addObject:artist];
            [_artists_pending_add addObject:artist];
        }
        return artist;
    }

    artist = [[CKRecord alloc] initWithRecordType:@"Artist"];
    artist[@"alias"] = alias;
    artist[@"name"] = name;
    if (_id)
        artist[@"id"] = _id;

    if (addToLibrary) {
        [_artists addObject:artist];
        [_artists_pending_add addObject:artist];
    }
    else
        [_cachedArtists addObject:artist];

   // NSLog(@"Imported: %@", artist);

    return artist;
}

- (Promise* _Nonnull)importModuleFromFileURL:(NSURL*)fileURL addToLibrary:(BOOL)addToLibrary {
    NSData* data = [NSData dataWithContentsOfURL:fileURL];
    if (!data)
        return [Promise rejected:nil];
    
    NSString* hash = [data MD5];
    __block CKRecord* module = [self findModuleByHash:hash];
    
    // Got the module already!
    if (module) {
        // No asset connected to the module, but since we have it locally, just add it
        if (!module[@"data"]) {
            NSURL* tmp = [self fileURLToData:module];
            [data writeToURL:tmp atomically:YES];
            module[@"data"] = [[CKAsset alloc] initWithFileURL:tmp];
            if (addToLibrary) {
                [_modules_pending_add addObject:module];
            }
        }
        return [Promise resolved:module];
    }
    else {
        Deferred* deferred = [Deferred deferred];

        // Try find it on ModArchive
        __block NSDictionary* moduleInfo = nil;
        [[[[ModArchive sharedInstance] getModuleByHash:hash] onMainQueue] when:^(NSDictionary* result) {
            moduleInfo = result[@"modarchive"][@"module"];
        } failed:^(NSError *error) {
            NSLog(@"%@", error);
        } any:^{
            // On ModArchive!
            if (moduleInfo) {
                module = [self importModuleFromModArchive:moduleInfo addToLibrary:addToLibrary];
                
                NSURL* tmp = [self fileURLToData:module];
                [data writeToURL:tmp atomically:YES];
                module[@"data"] = [[CKAsset alloc] initWithFileURL:tmp];
                [deferred resolve:module];
                if (addToLibrary) {
                    [_modules_pending_add addObject:module];
                }
            } else {
                // Not on ModArchive, generate record from the module itself
                openmpt_module* mod = openmpt_module_create_from_memory((void*)data.bytes, data.length, NULL, NULL, NULL);
                if (!mod)
                    [deferred reject:nil];
                else {
                    module = [[CKRecord alloc] initWithRecordType:@"Module"];
                    module[@"title"] = [Utilities titleStringForOpenMPTModule:mod];
                    module[@"hash"] = hash;
                    module[@"filename"] = [[fileURL path] lastPathComponent];
                    module[@"format"] = [[NSString stringWithCString:openmpt_module_get_metadata(mod, "type") encoding:NSASCIIStringEncoding] uppercaseString];
                    
                    NSURL* tmp = [self fileURLToData:module];
                    [data writeToURL:tmp atomically:YES];
                    module[@"data"] = [[CKAsset alloc] initWithFileURL:tmp];
                    openmpt_module_destroy(mod);

                    [deferred resolve:module];
                    if (addToLibrary) {
                        [_modules addObject:module];
                        [_modules_pending_add addObject:module];
                    }
                }
            }
        }];

        return deferred.promise;
    }
}

- (CKRecord*)importModuleFromModArchive:(NSDictionary*)moduleInfo addToLibrary:(BOOL)addToLibrary {
    CKRecord* module = [self findModuleByHash:moduleInfo[@"hash"]];
    if (module)
        return module;

    module = [self findCachedModuleByHash:moduleInfo[@"hash"]];
    if (module) {
        if (addToLibrary) {
            // TODO: Iterate artists here and make sure they are also added to the library.
            [_modules addObject:module];
            [_modules_pending_add addObject:module];
        }
        return module;
    }

    module = [[CKRecord alloc] initWithRecordType:@"Module"];
    module[@"title"] = [Utilities titleStringForModule:moduleInfo];
    module[@"id"] = moduleInfo[@"id"];
    module[@"hash"] = moduleInfo[@"hash"];
    module[@"filename"] = moduleInfo[@"filename"];
    module[@"format"] = moduleInfo[@"format"];

    NSMutableArray* artistReferences = [NSMutableArray array];
    NSArray<NSString*>* artists = [Utilities artistsForModule:moduleInfo includeGuessed:YES];
    for (NSDictionary* artist in artists) {
        [artistReferences addObject:[[CKReference alloc] initWithRecord:[self importArtistFromModArchive:artist
                                                                                            addToLibrary:addToLibrary]
                                                                 action:CKReferenceActionNone]
         ];
    }
    module[@"artists"] = [artistReferences copy];

    if (addToLibrary) {
        [_modules addObject:module];
        [_modules_pending_add addObject:module];
    } else {
        [_cachedModules addObject:module];
    }
    
   // NSLog(@"Imported: %@", module);

    return module;
}

- (NSURL*)fileURLToData:(CKRecord*)record {
    NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *filename = record[@"filename"];
    NSString *ext = [filename pathExtension];
    NSString *path = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", record[@"hash"], ext]];
    return [NSURL fileURLWithPath:path];
}

- (Promise*)getDataFromModule:(CKRecord*)record addToLibrary:(BOOL)addToLibrary {
    NSURL* localFile = [self fileURLToData:record];
    NSData* data = [NSData dataWithContentsOfURL:localFile];
    
    // Got local data
    if (data) {
        NSLog(@"Module %@ found locally", record[@"filename"]);
        
        CKAsset* asset = record[@"data"];
        if (!asset && addToLibrary) {
            NSLog(@"Not in iCloud, saving to iCloud");
            CKAsset* asset = [[CKAsset alloc] initWithFileURL:localFile];
            record[@"data"] = asset;
            [_modules_pending_add addObject:record];
        }
        
        return [Promise resolved:data];
    }
    else {
        CKAsset* asset = record[@"data"];
        
        // Got data from iCloud
        if (asset) {
            NSLog(@"Module %@ found in iCloud, downloading", record[@"filename"]);

            // Sync locally
            data = [NSData dataWithContentsOfURL:asset.fileURL];
            [data writeToURL:localFile atomically:YES];
            return [Promise resolved:data];
        } else {
            Deferred* deferred = [Deferred deferred];
            
            // Download from ModArchive
            NSLog(@"Downloading module %@ from ModArchive", record[@"filename"]);
            [[[[ModArchive sharedInstance] downloadModuleById:record[@"id"]] onMainQueue] when:^(NSURL* location) {
                NSLog(@"Module %@ downloaded", record[@"filename"]);
                
                NSLog(@"Saving locally");
                NSData* data = [NSData dataWithContentsOfURL:location];
                [data writeToURL:localFile atomically:YES];
                [deferred resolve:data];

                if (addToLibrary) {
                    NSLog(@"Saving to iCloud");
                    CKAsset* asset = [[CKAsset alloc] initWithFileURL:localFile];
                    record[@"data"] = asset;
                    [_modules_pending_add addObject:record];
                }
            } failed:^(NSError *error) {
                NSLog(@"%@", error);
                [deferred reject:error];
            }];
            
            return deferred.promise;
        }
    }
}

- (void)sync {
    [self localSync];

    CKDatabase* database = [CKContainer defaultContainer].privateCloudDatabase;

    CKModifyRecordsOperation* op = [[CKModifyRecordsOperation alloc] init];

    NSMutableArray* tmp = [NSMutableArray array];
    [tmp addObjectsFromArray:_artists_pending_add];
    [tmp addObjectsFromArray:_modules_pending_add];
    [tmp addObjectsFromArray:_playlists_pending_add];
    [_artists_pending_add removeAllObjects];
    [_modules_pending_add removeAllObjects];
    [_playlists_pending_add removeAllObjects];
    NSMutableDictionary* uniqueSaves = [NSMutableDictionary dictionary];
    for (CKRecord* record in tmp)
        [uniqueSaves setObject:record forKey:record.recordID.recordName];
    NSArray* saves = [uniqueSaves allValues];
    NSLog(@"Pending saves: %lu", (unsigned long)saves.count);

    tmp = [NSMutableArray array];
    [tmp addObjectsFromArray:_playlistsIDs_pending_delete];
    [tmp addObjectsFromArray:_modulesIDs_pending_delete];
    [tmp addObjectsFromArray:_artistsIDs_pending_delete];
    [_artistsIDs_pending_delete removeAllObjects];
    [_modulesIDs_pending_delete removeAllObjects];
    [_playlistsIDs_pending_delete removeAllObjects];
    NSMutableDictionary* uniqueDeletes = [NSMutableDictionary dictionary];
    for (CKRecordID* recordID in tmp)
        [uniqueDeletes setObject:recordID forKey:recordID.recordName];
    NSArray* deletes = [uniqueDeletes allValues];
    NSLog(@"Pending deletes: %lu", (unsigned long)deletes.count);
    
    op.recordsToSave = saves;
    op.recordIDsToDelete = deletes;
    op.savePolicy = CKRecordSaveAllKeys;
    op.modifyRecordsCompletionBlock = ^(NSArray<CKRecord *> * _Nullable savedRecords, NSArray<CKRecordID *> * _Nullable deletedRecordIDs, NSError * _Nullable operationError) {
        NSLog(@"Synced with error: %@", operationError);
        if (!operationError)
            [self localSync];
        [self cloudSync];
    };

    [database addOperation:op];
}

- (void) removePlaylistFromLibrary:(Playlist* _Nonnull)playlist {
    CKRecord* record = playlist.record;

    NSPredicate* filter = [NSPredicate predicateWithFormat:@"recordID != %@", playlist.record.recordID];
    [_playlists filterUsingPredicate:filter];
    [_playlistsIDs_pending_delete addObject:record.recordID];
}

- (void) updatePlaylistInLibrary:(Playlist* _Nonnull)playlist {
    CKRecord* record = playlist.record;
    
    NSPredicate* filter = [NSPredicate predicateWithFormat:@"recordID != %@", playlist.record.recordID];
    [_playlists filterUsingPredicate:filter];
    [_playlists addObject:record];
    [_playlists_pending_add addObject:record];
}


- (void)localSync {
    NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    [NSKeyedArchiver archiveRootObject:_artists
                                toFile:[documentDir stringByAppendingPathComponent:@"artists.db"]];
    [NSKeyedArchiver archiveRootObject:_modules
                                toFile:[documentDir stringByAppendingPathComponent:@"modules.db"]];
    [NSKeyedArchiver archiveRootObject:_playlists
                                toFile:[documentDir stringByAppendingPathComponent:@"playlists.db"]];
    [NSKeyedArchiver archiveRootObject:_artists_pending_add
                                toFile:[documentDir stringByAppendingPathComponent:@"artists_pending_add.db"]];
    [NSKeyedArchiver archiveRootObject:_modules_pending_add
                                toFile:[documentDir stringByAppendingPathComponent:@"modules_pending_add.db"]];
    [NSKeyedArchiver archiveRootObject:_playlists_pending_add
                                toFile:[documentDir stringByAppendingPathComponent:@"playlists_pending_add.db"]];
    [NSKeyedArchiver archiveRootObject:_artistsIDs_pending_delete
                                toFile:[documentDir stringByAppendingPathComponent:@"artists_pending_delete.db"]];
    [NSKeyedArchiver archiveRootObject:_modulesIDs_pending_delete
                                toFile:[documentDir stringByAppendingPathComponent:@"modules_pending_delete.db"]];
    [NSKeyedArchiver archiveRootObject:_playlistsIDs_pending_delete
                                toFile:[documentDir stringByAppendingPathComponent:@"playlists_pending_delete.db"]];
}

- (void)localInit {
    NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    _artists = [NSKeyedUnarchiver unarchiveObjectWithFile:
                [documentDir stringByAppendingPathComponent:@"artists.db"]];
    _modules = [NSKeyedUnarchiver unarchiveObjectWithFile:
                [documentDir stringByAppendingPathComponent:@"modules.db"]];
    _playlists = [NSKeyedUnarchiver unarchiveObjectWithFile:
                  [documentDir stringByAppendingPathComponent:@"playlists.db"]];
    _artists_pending_add = [NSKeyedUnarchiver unarchiveObjectWithFile:
                            [documentDir stringByAppendingPathComponent:@"artists_pending_add.db"]];
    _modules_pending_add = [NSKeyedUnarchiver unarchiveObjectWithFile:
                            [documentDir stringByAppendingPathComponent:@"modules_pending_add.db"]];
    _playlists_pending_add = [NSKeyedUnarchiver unarchiveObjectWithFile:
                              [documentDir stringByAppendingPathComponent:@"playlists_pending_add.db"]];
    _artistsIDs_pending_delete = [NSKeyedUnarchiver unarchiveObjectWithFile:
                                  [documentDir stringByAppendingPathComponent:@"artists_pending_delete.db"]];
    _modulesIDs_pending_delete = [NSKeyedUnarchiver unarchiveObjectWithFile:
                                  [documentDir stringByAppendingPathComponent:@"modules_pending_delete.db"]];
    _playlistsIDs_pending_delete = [NSKeyedUnarchiver unarchiveObjectWithFile:
                                    [documentDir stringByAppendingPathComponent:@"playlists_pending_delete.db"]];

    NSLog(@"_artists: %lu", (unsigned long)_artists.count);
    NSLog(@"_modules: %lu", (unsigned long)_modules.count);
    NSLog(@"_playlists: %lu", (unsigned long)_playlists.count);
    NSLog(@"_artists_pending_add: %lu", (unsigned long)_artists_pending_add.count);
    NSLog(@"_modules_pending_add: %lu", (unsigned long)_modules_pending_add.count);
    NSLog(@"_playlists_pending_add: %lu", (unsigned long)_playlists_pending_add.count);
    NSLog(@"_artistsIDs_pending_delete: %lu", (unsigned long)_artistsIDs_pending_delete.count);
    NSLog(@"_modulesIDs_pending_delete: %lu", (unsigned long)_modulesIDs_pending_delete.count);
    NSLog(@"_playlistsIDs_pending_delete: %lu", (unsigned long)_playlistsIDs_pending_delete.count);
}

- (void)cloudSync {
    NSPredicate* truePredicate = [NSPredicate predicateWithValue:YES];
    Promise* partists = [self fetchRecordsOfType:@"Artist" usingPredicate:truePredicate fullRecords:NO];
    Promise* pmodules = [self fetchRecordsOfType:@"Module" usingPredicate:truePredicate fullRecords:NO];
    Promise* pplaylists = [self fetchRecordsOfType:@"Playlist" usingPredicate:truePredicate fullRecords:NO];
    
    [[Promise and:@[partists, pmodules, pplaylists]] when:^(id result) {
        NSSet* cloudArtists = [NSSet setWithArray:[partists.result valueForKey:@"recordID"]];
        NSSet* cloudModules = [NSSet setWithArray:[pmodules.result valueForKey:@"recordID"]];
        NSSet* cloudPlaylists = [NSSet setWithArray:[pplaylists.result valueForKey:@"recordID"]];
        NSLog(@"Cloud artists: %lu", (unsigned long)cloudArtists.count);
        NSLog(@"Cloud modules: %lu", (unsigned long)cloudModules.count);
        NSLog(@"Cloud playlists: %lu", (unsigned long)cloudPlaylists.count);

        NSSet* localArtists = [NSSet setWithArray:[_artists valueForKey:@"recordID"]];
        NSSet* localModules = [NSSet setWithArray:[_modules valueForKey:@"recordID"]];
        NSSet* localPlaylists = [NSSet setWithArray:[_playlists valueForKey:@"recordID"]];
        NSLog(@"Local artists: %lu", (unsigned long)localArtists.count);
        NSLog(@"Local modules: %lu", (unsigned long)localModules.count);
        NSLog(@"Local playlists: %lu", (unsigned long)localPlaylists.count);
        
        NSMutableSet* missingArtists = [cloudArtists mutableCopy]; [missingArtists minusSet:localArtists];
        NSMutableSet* missingModules = [cloudModules mutableCopy]; [missingModules minusSet:localModules];
        NSMutableSet* missingPlaylist = [cloudPlaylists mutableCopy]; [missingPlaylist minusSet:localPlaylists];
        NSLog(@"Missing artists: %lu", (unsigned long)missingArtists.count);
        NSLog(@"Missing modules: %lu", (unsigned long)missingModules.count);
        NSLog(@"Missing playlists: %lu", (unsigned long)missingPlaylist.count);

        NSMutableSet* deletedArtists = [localArtists mutableCopy]; [deletedArtists minusSet:cloudArtists];
        NSMutableSet* deletedModules = [localModules mutableCopy]; [deletedModules minusSet:cloudModules];
        NSMutableSet* deletedPlaylist = [localPlaylists mutableCopy]; [deletedPlaylist minusSet:cloudPlaylists];
        NSLog(@"Deleted artists: %lu", (unsigned long)deletedArtists.count);
        NSLog(@"Deleted modules: %lu", (unsigned long)deletedModules.count);
        NSLog(@"Deleted playlists: %lu", (unsigned long)deletedPlaylist.count);

        NSArray* test = [_modules filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (recordID IN %@)", deletedModules]];
        NSLog(@"Delete: %lu -> %lu", (unsigned long)_modules.count, test.count);
        NSLog(@"%lu", (unsigned long)((NSArray*)pmodules.result).count);

        [_readyDeferred resolve:nil];
    } failed:^(NSError *error) {
        NSLog(@"Cloud sync failed with error: %@", error);
        [_readyDeferred reject:error];
    }];
}

- (id)init {
    self = [super init];
    
    _readyDeferred = [Deferred deferred];

    [self localInit];
    
    if (!_modules)
        _modules = [NSMutableArray array];
    if (!_artists)
        _artists = [NSMutableArray array];
    if (!_playlists)
        _playlists = [NSMutableArray array];
    if (!_modules_pending_add)
        _modules_pending_add = [NSMutableArray array];
    if (!_artists_pending_add)
        _artists_pending_add = [NSMutableArray array];
    if (!_playlists_pending_add)
        _playlists_pending_add = [NSMutableArray array];
    if (!_modulesIDs_pending_delete)
        _modulesIDs_pending_delete = [NSMutableArray array];
    if (!_artistsIDs_pending_delete)
        _artistsIDs_pending_delete = [NSMutableArray array];
    if (!_playlistsIDs_pending_delete)
        _playlistsIDs_pending_delete = [NSMutableArray array];
    
    _cachedArtists = [NSMutableArray array];
    _cachedModules = [NSMutableArray array];
    
    [self sync];
    
    return self;
}

@end
