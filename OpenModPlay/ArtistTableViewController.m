//
//  ArtistTableViewController.m
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-14.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import "ArtistTableViewController.h"
#import "ModArchive.h"
#import "Utilities.h"
#import "SongTableViewCell.h"
#import "Database.h"
#import "Playback.h"

@implementation ArtistTableViewController {
    NSArray* modules;
    Playlist* playlist;
    id stateChangeObserver;
    id newModuleObserver;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    modules = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [[[[ModArchive sharedInstance] modulesByArtist:_artistID page:0] onMainQueue] when:^(id results) {
        id tmp = results[@"modarchive"][@"module"];
        if (tmp == nil)
            modules = nil;
        else if ([tmp isKindOfClass:[NSArray class]])
            modules = tmp;
        else
            modules = @[tmp];
    } failed:^(NSError *error) {
        NSLog(@"%@", error);
    } any:^{
        [Utilities generateModuleTitles:[modules mutableCopy]];
        modules = [Utilities sortModulesByTitle:modules];
        
        playlist = [[Playlist alloc] initWithName:[NSString stringWithFormat:@"modules by %@", _name]];
        for (NSDictionary* moduleInfo in modules) {
            CKRecord* module = [[Database sharedInstance] importModuleFromModArchive:moduleInfo addToLibrary:NO];
            [playlist.list addObject:module];
        }

        [self.tableView reloadData];
    }];
    
    stateChangeObserver = [[NSNotificationCenter defaultCenter] addObserverForName:PlaybackStateChangedNotification object:[Playback sharedInstance] queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        [self.tableView reloadData];
    }];
    
    newModuleObserver = [[NSNotificationCenter defaultCenter] addObserverForName:PlaybackNewModuleNotification object:[Playback sharedInstance] queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        [self.tableView reloadData];
    }];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
//    [self.navigationController setNavigationBarHidden:NO animated:animated];

    if (stateChangeObserver) {
        stateChangeObserver = nil;
        [[NSNotificationCenter defaultCenter] removeObserver:stateChangeObserver];
    }
    
    if (newModuleObserver) {
        newModuleObserver = nil;
        [[NSNotificationCenter defaultCenter] removeObserver:newModuleObserver];
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return modules?1:0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return modules.count;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Songs";
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.textColor = [UIColor whiteColor];
    header.textLabel.font = [UIFont boldSystemFontOfSize:18];
    CGRect headerFrame = header.frame;
    header.textLabel.frame = headerFrame;
    header.textLabel.textAlignment = NSTextAlignmentCenter;
    header.textLabel.text = [header.textLabel.text capitalizedString];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SongTableViewCell* stvc = [tableView dequeueReusableCellWithIdentifier:@"SONG"];
    stvc.viewController = self;
    stvc.currentArtistId = _artistID;
    stvc.module = playlist.list[indexPath.row];
    return stvc;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[Playback sharedInstance] setPlaylist:playlist position:indexPath.row];
    [[Playback sharedInstance] play];
}

@end
