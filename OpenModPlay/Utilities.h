//
//  Utilities.h
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-15.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CloudKit/CloudKit.h>
#include <libopenmpt/libopenmpt.h>

@interface Utilities : NSObject

+ (NSString*) artistStringForModule:(NSDictionary*)moduleInfo;
+ (NSString*) artistStringForModuleRecord:(CKRecord*)moduleRecord;
+ (NSArray<NSString*>*) artistsForModule:(NSDictionary*)moduleInfo includeGuessed:(BOOL)guessed;
+ (NSString*) titleStringForModule:(NSDictionary*)moduleInfo;
+ (NSString*) artistStringForOpenMPTModule:(openmpt_module *)module;
+ (NSString*) titleStringForOpenMPTModule:(openmpt_module*)module;
+ (NSString*) nameStringForArtist:(NSDictionary *)artistInfo;
+ (UIImage*) albumArtForModule:(openmpt_module*)module record:(CKRecord*)record;
+ (NSString*) removeHTMLinString:(NSString*)string;
+ (NSArray*) sortModulesByTitle:(NSArray*)modules;
+ (NSArray*) sortArtistsByName:(NSArray*)artists;
+ (void) generateModuleTitles:(NSMutableArray*)modules;
+ (void) generateArtistNames:(NSMutableArray*)artists;

@end

