//
//  AlertAction.m
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-15.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import "AlertAction.h"

@implementation AlertAction {
    void (^_handler)(AlertAction * _Nullable);
}

+ (instancetype _Nonnull )actionWithTitle:(nullable NSString *)title image:(nullable UIImage*)image handler:(void (^ __nullable)(AlertAction * _Nullable action))handler {
    AlertAction* action = [[AlertAction alloc] init];
    action->_title = title;
    action->_image = image;
    action->_handler = handler;
    return action;
}

@end
