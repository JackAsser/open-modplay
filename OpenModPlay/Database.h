//
//  Database.h
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-15.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import <CloudKit/CloudKit.h>
#import <Foundation/Foundation.h>
#import "Promise.h"
#import "Playlist.h"

@interface Database : NSObject

@property (readonly, nonatomic, nonnull) NSArray* artists;
@property (readonly, nonatomic, nonnull) NSArray* modules;
@property (readonly, nonatomic, nonnull) NSArray* playlists;

+ (instancetype _Nonnull)sharedInstance;

- (Promise* _Nonnull)ready;
- (BOOL)modArchiveModuleExists:(NSDictionary* _Nonnull)moduleInfo;
- (CKRecord* _Nonnull)importModuleFromModArchive:(NSDictionary* _Nonnull)moduleInfo addToLibrary:(BOOL)addToLibrary;
- (Promise* _Nonnull)importModuleFromFileURL:(NSURL* _Nonnull)fileURL addToLibrary:(BOOL)addToLibrary;
- (Promise* _Nonnull)getDataFromModule:(CKRecord* _Nonnull)record addToLibrary:(BOOL)addToLibrary;
- (void) sync;

- (CKRecord* _Nullable)findModuleByRecordID:(CKRecordID* _Nonnull)recordID;
- (CKRecord* _Nullable)findModuleByHash:(NSString* _Nonnull)hash;
- (CKRecord* _Nullable)findArtistByRecordID:(CKRecordID* _Nonnull)recordID;
- (CKRecord* _Nullable)findArtistById:(NSString* _Nonnull)_id;
- (CKRecord* _Nullable)findArtistByAlias:(NSString* _Nonnull)alias;

- (void) removePlaylistFromLibrary:(Playlist* _Nonnull)playlist;
- (void) updatePlaylistInLibrary:(Playlist* _Nonnull)playlist;

@end
