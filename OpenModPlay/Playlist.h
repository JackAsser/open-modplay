//
//  Playlist.h
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-19.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//
@import Foundation;
@import CloudKit;

@interface Playlist : NSObject

@property (readwrite, nonnull) NSString* name;
@property (readonly, nonnull) NSMutableArray<CKRecord*>* list;
@property (readonly, nonatomic, nonnull) CKRecord* record;

-(instancetype _Nonnull)initWithName:(NSString* _Nonnull)name;
-(instancetype _Nonnull)initFromCloudPlaylist:(CKRecord* _Nonnull)playlist;

@end
