//
//  PopupViewController.m
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-12.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import "PopupViewController.h"

@implementation PopupViewController {
    NSLayoutConstraint* topConstraint;
    float collapsedPosition;
    CGRect tabBarFrame;
    BOOL collapsed;
    IBOutlet UIView* statusBarView;
    IBOutlet UIView* playerView;
}

- (void)willMoveToParentViewController:(UIViewController *)parent {
    [super willMoveToParentViewController:parent];
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view.widthAnchor constraintEqualToAnchor:parent.view.widthAnchor].active = YES;
    [self.view.heightAnchor constraintEqualToAnchor:parent.view.heightAnchor].active = YES;
    [self.view.leftAnchor constraintEqualToAnchor:parent.view.leftAnchor].active = YES;
    collapsedPosition = parent.view.frame.size.height - self.tabBarController.tabBar.frame.size.height;// - 44;
    topConstraint = [self.view.topAnchor constraintEqualToAnchor:parent.view.topAnchor
                                                        constant:collapsedPosition];
    topConstraint.active = YES;
    [self.view.superview layoutIfNeeded];
    
    collapsedPosition-=44;
    topConstraint.constant = collapsedPosition;
    [UIView animateWithDuration:0.4 animations:^{
        [self.view.superview layoutIfNeeded];
    }];
    
    tabBarFrame = self.tabBarController.tabBar.frame;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    collapsed = YES;
    playerView.alpha = 0;
    statusBarView.alpha = 1;
}

- (void)expand {
    [self.view.superview layoutIfNeeded];
    collapsedPosition = 0;
    topConstraint.constant = collapsedPosition;
    collapsed = NO;
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    
    [UIView animateWithDuration:0.2 animations:^{
        playerView.alpha = 1;
        statusBarView.alpha = 0;
        self.tabBarController.tabBar.frame = CGRectOffset(tabBarFrame, 0, tabBarFrame.size.height);
        [self.view.superview layoutIfNeeded];
    }];
}

- (void)collapse {
    [self.view.superview layoutIfNeeded];
    collapsedPosition = self.view.superview.frame.size.height - self.tabBarController.tabBar.frame.size.height - 44;
    topConstraint.constant = collapsedPosition;
    collapsed = YES;
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    
    [UIView animateWithDuration:0.2 animations:^{
        playerView.alpha = 0;
        statusBarView.alpha = 1;
        self.tabBarController.tabBar.frame = CGRectOffset(tabBarFrame, 0, 0);
        [self.view.superview layoutIfNeeded];
    }];
}

- (void)handleDrag:(UIPanGestureRecognizer *)recognizer {
    CGPoint translation = [recognizer translationInView:self.view.superview];
    float y = collapsedPosition + translation.y;
    if (y<0)
        y=0;
    float maxy = self.view.superview.frame.size.height - self.tabBarController.tabBar.frame.size.height - 44;
    if (y>maxy)
        y=maxy;
    
    float t = y/maxy;
    
    float mid = 0.95;
    float mid2 = 0.92;
    statusBarView.alpha = 1-fmax(0, fmin(1, (1-t) / (1.0-mid)));
    playerView.alpha = fmax(0, fmin(1, (mid-t) / (1.0-mid2)));
    
    if (recognizer.state == UIGestureRecognizerStateChanged) {
        topConstraint.constant = y;
        self.tabBarController.tabBar.frame = CGRectOffset(tabBarFrame, 0, tabBarFrame.size.height*(1-t));
    }
    else if (recognizer.state == UIGestureRecognizerStateEnded) {
        float vy = [recognizer velocityInView:self.view.superview].y;
        if (vy<-1000)
            [self expand];
        else if (vy>1000)
            [self collapse];
        else if (t<0.7)
            [self expand];
        else
            [self collapse];
    }
}

@end
