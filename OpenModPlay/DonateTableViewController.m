//
//  DonateTableViewController.m
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-24.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import "DonateTableViewController.h"

@implementation DonateTableViewController {
    NSSet* productIDs;
    NSArray<SKProduct *>* products;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    productIDs = [NSSet setWithArray:@[@"PLATINUM", @"GOLD", @"SILVER", @"BRONZE"]];
    SKProductsRequest* request = [[SKProductsRequest alloc] initWithProductIdentifiers:productIDs];
    request.delegate = self;
    [request start];

    UIViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DONATE_HEADER"];
    vc.view.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableView.tableHeaderView = vc.view;
    vc.view.backgroundColor = [UIColor clearColor];
    [vc.view.widthAnchor constraintEqualToAnchor:self.tableView.widthAnchor].active = YES;
    [vc.view.topAnchor constraintEqualToAnchor:self.tableView.topAnchor].active = YES;
    [vc.view.heightAnchor constraintEqualToConstant:50].active = YES;
    self.tableView.contentInset = UIEdgeInsetsMake(200, 0, 0, 0);
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    products = [response.products sortedArrayUsingDescriptors:@[
                                                                [NSSortDescriptor sortDescriptorWithKey:@"price"
                                                                                              ascending:YES]
                                                                ]];
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return products ? 1 : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return products.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DONATE_CELL" forIndexPath:indexPath];

    SKProduct* product = products[indexPath.row];

    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:product.priceLocale];
    NSString *formattedPrice = [numberFormatter stringFromNumber:product.price];

    cell.textLabel.text = product.localizedTitle;
    cell.detailTextLabel.text = formattedPrice;
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.textColor = [UIColor orangeColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

@end
