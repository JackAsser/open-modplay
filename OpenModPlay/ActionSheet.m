//
//  ActionSheet.m
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-15.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import "ActionSheet.h"

@implementation ActionSheet {
    IBOutlet UITableView* tableView;
    NSMutableArray<AlertAction*>* actions;
    IBOutlet UIView* cancelView;
}

#define ROW_HEIGHT 60

- (id)initWithCoder:(NSCoder *)aDecoder {
    actions = [NSMutableArray array];
    return [super initWithCoder:aDecoder];
}

- (void)addAction:(AlertAction *)action {
    [actions addObject:action];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    float tableHeight = (actions.count+1)*ROW_HEIGHT;
    float viewHeight = self.view.bounds.size.height;
    float padding = viewHeight - tableHeight - cancelView.bounds.size.height;
    
    tableView.contentInset = UIEdgeInsetsMake(viewHeight,0,0,0);
    [UIView animateWithDuration:0.3 animations:^{
        tableView.contentInset = UIEdgeInsetsMake(padding,0,padding,0);
    }];
}

- (IBAction)cancelPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return actions.count+1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return ROW_HEIGHT;
}

-(UITableViewCell*)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell;
    if (indexPath.row == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"SONG"];
        cell.textLabel.text = self.title;
        cell.detailTextLabel.text = _subTitle;
        if (_image)
            cell.imageView.image = _image;
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"ACTION"];
        NSInteger index = indexPath.row-1;
        
        AlertAction* action = actions[index];
        cell.textLabel.text = action.title;
        if (action.image) {
            UIImage* image = [action.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cell.imageView.image = image;
            cell.imageView.tintColor = [UIColor lightGrayColor];
        }
    }

    cell.textLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

@end
