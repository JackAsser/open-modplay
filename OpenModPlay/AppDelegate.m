//
//  AppDelegate.m
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-11.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import "AppDelegate.h"
#import "Playback.h"
#import "ModArchive.h"
#import "Database.h"

@implementation AppDelegate

- (void)applyDefaultStyling {
    [[UITableView appearance] setBackgroundColor:[UIColor colorWithRed:24.0/255.0
                                                                 green:24.0/255.0
                                                                  blue:24.0/255.0
                                                                 alpha:1.0]];
    [[UITableView appearance] setTintColor:[UIColor colorWithWhite:0.8 alpha:1.0]];
    [[UITableView appearance] setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [[UINavigationBar appearance] setTranslucent:YES];
    [[UINavigationBar appearance] setBarStyle:UIBarStyleBlack];
    
    [[UINavigationBar appearance] setTintColor:[UIColor orangeColor]];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
}

- (void)generateInitialData {
    
    NSArray* moduleNames = @[@"enigma.mod",
                             @"ALLISTER BRIMBLE - Project X (Title) Bladswede Remix.mod",
                             @"ANTIBRAIN - Television.mod",
                             @"AZAZEL - Fracture.mod",
                             @"BARRY LEITCH - Lotus 2 (Title).mod",
                             @"BASSBOMB - Operation September.mod",
                             @"BIONIC - Pure Motion.mod",
                             @"BRAINBUG - Shinin Ol Boots.mod",
                             @"BRAINTUMOUR & HEYWOOD - Jammin In The Wind.mod",
                             @"CELSIUS - After Taste.mod",
                             @"CELSIUS - Giveindizbiz.mod",
                             @"CELSIUS - The Live Act 1.mod",
                             @"CELSIUS - The Live Act 2.mod",
                             @"CHORUS & SID - Killing Floor 2.1.mod",
                             @"CHROMAG - Switchback (Main).mod",
                             @"COUNTERPOINT - Standing Close.mod",
                             @"CRUNCH - Brutal.mod",
                             @"DAVID LINDSTROM - Aluminium....mod",
                             @"DAXX & TURBO B - Capturing Matrix.mod",
                             @"DAXX - 95 Ravers Megamix.mod",
                             @"DAXX - Children 165bpm.mod",
                             @"DAXX - Insenity.mod",
                             @"DAXX - Lockn Up Me Now.mod",
                             @"DEAF AID - Universal Raver.mod",
                             @"DEXTER - Big Throng.mod",
                             @"DJ THIN - Go Get Busy.mod",
                             @"DOQ - Hardcore World.mod",
                             @"DR AWESOME - Blood On The R-Top.mod",
                             @"ECHO - Cybernet.mod",
                             @"GINSENG - Let it Rip.mod",
                             @"GREG - Odyssey Part1.mod",
                             @"GREG - Odyssey Part2.mod",
                             @"GREG - Odyssey Part3.mod",
                             @"GREG - Odyssey Part4.mod",
                             @"GREG - Odyssey Part5.mod",
                             @"GREG - Odyssey Part6.mod",
                             @"HEATBEAT - Sainahi Circles (Original).mod",
                             @"HEATBEAT - Street Jungle.mod",
                             @"HOLLYWOOD - Spilt Milk.mod",
                             @"INTERPHACE - Phunq Ethereal.mod",
                             @"JESPER KYD - Global Trash III v2.mod",
                             @"JESTER - Elysium.mod",
                             @"JESTER - Molecules Revenge.mod",
                             @"JESTER - Wizardry.mod",
                             @"JOGEIR LILJEDAHL - G-Comp.mod",
                             @"JOGEIR LILJEDAHL - Guitar Slinger.mod",
                             @"JOGEIR LILJEDAHL - Physical Presence.mod",
                             @"JOGEIR LILJEDAHL - Signia.mod",
                             @"JUICE - Da Jungle Is Wicked.mod",
                             @"KEVIN COLLIER - Harley Music.mod",
                             @"LIZARDKING - World Of Insanity.mod",
                             @"MAD FREAK - 3d Demo II Theme.mod",
                             @"MARC - Generation Love.mod",
                             @"MARK KNIGHT - Acidic Lampshade.mod",
                             @"MARTIN IVESON - JagTitle.mod",
                             @"MC HILL - Allusion of Rave.mod",
                             @"MISTY & DAERON - Reverie of Truth.mod",
                             @"MOBY - Groovy Thing.mod",
                             @"MOBY - Knulla Kuk!.mod",
                             @"MOBY - Pelforth Blues.mod",
                             @"MR MAN - Distant Call.mod",
                             @"NEURODANCER - 4 Spirits.mod",
                             @"NEURODANCER - Chakral Frequency.mod",
                             @"OLOF GUSTAFSSON - Light of Day.mod",
                             @"OTIS - Live In Amsterdam.mod",
                             @"OTIS - Thunderdon.mod",
                             @"PHYTON - Love Hardcore.mod",
                             @"PIRAT - Parliament Of Dreams.mod",
                             @"PIRAT - Psychotic Angel.mod",
                             @"PRODIGY - Happy Hardcore.mod",
                             @"PUNNIK - De Die Hards.mod",
                             @"PUNNIK - Sorry !.mod",
                             @"PUNNIK - Vort Boojaart !.mod",
                             @"RANDALL - La Luna.mod",
                             @"RIB - Stardust (Title).mod",
                             @"ROBERTS - Music Voyage.mod",
                             @"ROMEO KNIGHT - Boesendorfer P S S.mod",
                             @"ROMEO KNIGHT - Piggies Hut.mod",
                             @"SCAVY & MINDFUCK - Dancing Samba.mod",
                             @"SCORPIK - Field Day.mod",
                             @"SHORTY & TIM - Will Of The Wind.mod",
                             @"SIDEWINDER - 2 Bad Sheep.mod",
                             @"SIDEWINDER - Spanish Armada.mod",
                             @"SIDEWINDER - The Scribe.mod",
                             @"SLICE - 57 Remix.mod",
                             @"SLICE - Technology.mod",
                             @"STATIC - Human Target.mod",
                             @"STEFAN UHLEMANN - Piano Lyrics.mod",
                             @"TEKKLESS SPACEHEAD - The Last Saga.mod",
                             @"TIM HAYWOOD - I Want You (Tim Buk 2 Rmx).mod",
                             @"TIM WRIGHT - Agony (Intro).mod",
                             @"TIME GUARDIAN - AG Techno.mod",
                             @"TIMER - Energetic.mod",
                             @"TONE C - Brainshock I.mod",
                             @"TONID - Slainte Mhor.mod",
                             @"TRAVOLTA - Condom Corruption.mod",
                             @"TRAVOLTA - Testlast (9 Fingers).mod",
                             @"UNREAL - Fountain Of Sighs.mod",
                             @"VESURI - BrainChoir.mod",
                             @"VESURI - Charly.mod",
                             @"VINNIE - The Groovy Element.mod",
                             @"VOCAL - Switchback (Endpart).mod",
                             @"X-CEED - Berlin 1931AD.mod",
                             @"XTD - Discovery.mod",
                             @"XTD - Reassurance.mod",
                             @"XTD - Stink Bomb.mod",
                             @"onward.xm"
                             ];
    
    static resolved_block handler;
    
    NSLog(@"Syncing with iCloud");
    [[[[Database sharedInstance] ready] onMainQueue] when:^(id result) {
        NSLog(@"Synced with iCloud");
        
        NSMutableArray<Promise*>* promises = [NSMutableArray array];
        NSMutableArray<NSString*>* files = [moduleNames mutableCopy];
        NSMutableArray<CKRecord*>* entries = [NSMutableArray array];
        handler = ^(id result) {
            for (Promise* p in promises) {
                if (p.result)
                    [entries addObject:(CKRecord*)p.result];
            }
            [promises removeAllObjects];
            
            NSLog(@"Syncing (%lu files left, %lu, %lu)...", files.count, entries.count, [Database sharedInstance].modules.count);
            
            while ((files.count>0) && (promises.count<10)) {
                NSString* path = [[NSBundle mainBundle] pathForResource:files[0] ofType:nil];
                NSURL* url = [NSURL fileURLWithPath:path];
                [promises addObject:[[Database sharedInstance] importModuleFromFileURL:url addToLibrary:YES]];
                [files removeObjectAtIndex:0];
            }
            
            if (promises.count > 0)
                [[[Promise all:promises] onMainQueue] when:handler];
            else {
                NSLog(@"Generating playlist...");
                Playlist* playlist = [[Playlist alloc] initWithName:@"Local files"];
                [playlist.list addObjectsFromArray:entries];
                [[Database sharedInstance] updatePlaylistInLibrary:playlist];
                [[Playback sharedInstance] setPlaylist:playlist];
                
                NSLog(@"Saving to iCloud...");
                [[Database sharedInstance] sync];
            }
        };
        handler(nil);
    } failed:^(NSError *error) {
        NSLog(@"Failed to sync with iCloud: %@", error);
    }];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //[self generateInitialData];
    CKRecord* tmp = [Database sharedInstance].playlists.firstObject;
    if (tmp) {
        Playlist* playlist = [[Playlist alloc] initFromCloudPlaylist:tmp];
        [[Playback sharedInstance] setPlaylist:playlist];
    } else {
        [[Database sharedInstance].ready when:^(id ready) {
            CKRecord* tmp = [Database sharedInstance].playlists.firstObject;
            if (tmp) {
                Playlist* playlist = [[Playlist alloc] initFromCloudPlaylist:tmp];
                [[Playback sharedInstance] setPlaylist:playlist];
            }
        }];
    }
    
    [application beginReceivingRemoteControlEvents];
    [application setStatusBarStyle:UIStatusBarStyleLightContent];

    [self applyDefaultStyling];
    
    return YES;
}

@end
