//
//  ActionSheet.h
//  OpenModPlay
//
//  Created by Andreas Larsson on 2017-04-15.
//  Copyright © 2017 Andreas Larsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertAction.h"

@interface ActionSheet : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (readwrite, nullable) NSString* subTitle;
@property (readwrite, nullable) UIImage* image;

- (void)addAction:(AlertAction*_Nonnull)action;

@end
